SELECT * FROM job;
SELECT * FROM project;
SELECT jobID, company_name FROM job;

DROP USER `portfoliouser`@`localhost`;
CREATE USER `portfoliouser`@`localhost` identified by 'portfoliopass';
#GRANT ALL PRIVILEGES ON `portfolio_db` .* To `portfoliouser`@`hostname`

REVOKE ALL PRIVILEGES ON portfolio_db.* FROM 'portfoliouser'@'hostname';

GRANT SELECT
ON portfolio_db .*
TO 'portfoliouser'@'localhost';

ALTER TABLE job DROP COLUMN menu_title;

ALTER TABLE project MODIFY project_download2_text VARCHAR(100);

ALTER TABLE job MODIFY job_title VARCHAR(100);

CREATE USER `portfolioadmin`@`localhost` identified by 'portfolio_pass';

GRANT ALL PRIVILEGES
ON portfolio_db .*
TO 'portfolioadmin'@'localhost';

ALTER TABLE job ADD company_logo_source VARCHAR(60);

ALTER TABLE placement ADD screenshot_source VARCHAR(60);

SELECT * FROM tag

SELECT * FROM team

INSERT INTO `project` (`projectID`, `project_title`, `screenshot_source`, `menu_section`, `menu_title`, `menu_thumbnail_source`, `project_description`, `project_please_note`, `project_download1_text`, `project_download1_source`, `project_download2_text`, `project_download2_source`) VALUES
(1, 'Astroids', '', 1, 'Astroids', 'source/sf', 'afhsdbfhs', '', '', '', '', ''),
(2, 'Game', '', 1, 'Game', 'sourdgsce/sf', 'afhsdbfhs', '', '', '', '', ''),
(3, 'Engine', '', 1, 'Engine', 'so33urce/sf', 'afhsdbfhs', '', '', '', '', ''),
(4, 'Monster Attack', '', 2, 'Monster Attack', 'soufrce/sf', 'afhsdbfhs', '', '', '', '', ''),
(5, 'Portfolio', '', 3, 'Portfolio', '11souvvrce/sf', 'afhsdbfhs', '', '', '', '', '');

INSERT INTO `job` (`jobID`, `company_name`, `job_title`, `dates`, `job_description`, `company_website`, `company_logo_source`) VALUES
(1, 'Motor Design Ltd.', 'Software Developer', 'June 2012 - September 2012; June 2013 - August 2014; June 2015 - August 2015', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris sed dignissim massa. Phasellus id rhoncus eros. Morbi dignissim pharetra mi, vehicula interdum felis malesuada nec. Morbi id turpis odio. Cras rhoncus ante ut leo fringilla tincidunt. Phasellus lacinia urna vel odio gravida cursus. Duis ut volutpat metus, sit amet gravida metus. Etiam vitae tellus efficitur, ultrices sem vel, euismod ex. Nunc eget sollicitudin velit. Nullam quis dapibus magna. Nullam id tempor nulla.</p><p>Ut pharetra dapibus sapien, placerat porttitor nunc tincidunt sit amet. Duis tristique consectetur pulvinar. Suspendisse vitae varius nisi. Donec molestie placerat ex ac tempor. Mauris consectetur ex velit, ac dignissim sapien mattis id. Cras consequat mattis sapien a sodales. Proin consectetur maximus ante sed dictum. Duis lorem quam, rhoncus at sapien sit amet, tempus sagittis nibh.</p>', 'http://www.motor-design.com', 'Motor_Design_logo.png'),
(2, 'The Access Group', 'Junior Application Developer', 'August 2015 - October 2015', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris sed dignissim massa. Phasellus id rhoncus eros. Morbi dignissim pharetra mi, vehicula interdum felis malesuada nec. Morbi id turpis odio. Cras rhoncus ante ut leo fringilla tincidunt. Phasellus lacinia urna vel odio gravida cursus. Duis ut volutpat metus, sit amet gravida metus. Etiam vitae tellus efficitur, ultrices sem vel, euismod ex. Nunc eget sollicitudin velit. Nullam quis dapibus magna. Nullam id tempor nulla.</p><p>Ut pharetra dapibus sapien, placerat porttitor nunc tincidunt sit amet. Duis tristique consectetur pulvinar. Suspendisse vitae varius nisi. Donec molestie placerat ex ac tempor. Mauris consectetur ex velit, ac dignissim sapien mattis id. Cras consequat mattis sapien a sodales. Proin consectetur maximus ante sed dictum. Duis lorem quam, rhoncus at sapien sit amet, tempus sagittis nibh.</p>', 'http://www.theaccessgroup.com', 'The_Access_Group_logo.svg'),
(3, 'IBM', 'Developer', 'October 2015 - Present', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris sed dignissim massa. Phasellus id rhoncus eros. Morbi dignissim pharetra mi, vehicula interdum felis malesuada nec. Morbi id turpis odio. Cras rhoncus ante ut leo fringilla tincidunt. Phasellus lacinia urna vel odio gravida cursus. Duis ut volutpat metus, sit amet gravida metus. Etiam vitae tellus efficitur, ultrices sem vel, euismod ex. Nunc eget sollicitudin velit. Nullam quis dapibus magna. Nullam id tempor nulla.</p><p>Ut pharetra dapibus sapien, placerat porttitor nunc tincidunt sit amet. Duis tristique consectetur pulvinar. Suspendisse vitae varius nisi. Donec molestie placerat ex ac tempor. Mauris consectetur ex velit, ac dignissim sapien mattis id. Cras consequat mattis sapien a sodales. Proin consectetur maximus ante sed dictum. Duis lorem quam, rhoncus at sapien sit amet, tempus sagittis nibh.</p>', 'http://www.ibm.com/uk-en/', 'IBM_logo.svg');

SELECT * FROM menu_section;



UPDATE placement
SET job_description='<P>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus hendrerit pulvinar lectus quis suscipit. Nunc sodales ac sem eu accumsan. Quisque at rhoncus felis, in dignissim justo. Vestibulum vitae tellus venenatis, venenatis ante sed, varius libero. Donec at arcu lobortis purus commodo ullamcorper vitae non nisl. Pellentesque bibendum id dolor et tincidunt. Cras erat felis, posuere malesuada efficitur ultricies, volutpat ut erat. Fusce vel felis vel tortor accumsan volutpat pulvinar a dui. Cras sem velit, viverra eget porttitor non, molestie quis erat. Morbi lacinia quis diam eget porta. Nulla venenatis rhoncus tellus nec blandit. Vivamus sit amet interdum sem, non bibendum odio.</P>

<P>Ut viverra lorem a hendrerit feugiat. Proin vel pharetra ex. Integer auctor nisi ut bibendum eleifend. Pellentesque bibendum nunc feugiat odio egestas volutpat. Ut eu tortor dui. Donec iaculis nisi vel nisl tristique pulvinar. Maecenas iaculis leo eu gravida elementum. Nunc justo mauris, cursus sit amet volutpat quis, facilisis ac magna. Phasellus viverra vehicula enim, quis faucibus tellus porta eu. Fusce malesuada massa dui, a pretium odio cursus eu. Duis mattis lacinia leo in congue. Morbi pulvinar blandit sollicitudin. Morbi ultricies iaculis nisl non egestas. Praesent faucibus ut erat vel interdum. Donec et porttitor odio. Aenean sodales arcu sit amet diam pharetra, ut rhoncus urna lacinia.</P>

<P>Morbi eget velit sit amet ante blandit convallis. Praesent id congue arcu. Ut nulla ligula, pharetra ut sem vitae, pretium molestie felis. In ut consectetur turpis. Mauris rhoncus bibendum malesuada. Interdum et malesuada fames ac ante ipsum primis in faucibus. Vestibulum aliquet leo id sapien rutrum vulputate. Proin a tincidunt nibh. Donec lacus arcu, aliquam vel dui eu, finibus dictum justo. Vivamus eu sagittis tellus, quis tristique enim. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</P>'
WHERE placementID < 5000; 

UPDATE project
SET project_description='<P>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus hendrerit pulvinar lectus quis suscipit. Nunc sodales ac sem eu accumsan. Quisque at rhoncus felis, in dignissim justo. Vestibulum vitae tellus venenatis, venenatis ante sed, varius libero. Donec at arcu lobortis purus commodo ullamcorper vitae non nisl. Pellentesque bibendum id dolor et tincidunt. Cras erat felis, posuere malesuada efficitur ultricies, volutpat ut erat. Fusce vel felis vel tortor accumsan volutpat pulvinar a dui. Cras sem velit, viverra eget porttitor non, molestie quis erat. Morbi lacinia quis diam eget porta. Nulla venenatis rhoncus tellus nec blandit. Vivamus sit amet interdum sem, non bibendum odio.</P>

<P>Ut viverra lorem a hendrerit feugiat. Proin vel pharetra ex. Integer auctor nisi ut bibendum eleifend. Pellentesque bibendum nunc feugiat odio egestas volutpat. Ut eu tortor dui. Donec iaculis nisi vel nisl tristique pulvinar. Maecenas iaculis leo eu gravida elementum. Nunc justo mauris, cursus sit amet volutpat quis, facilisis ac magna. Phasellus viverra vehicula enim, quis faucibus tellus porta eu. Fusce malesuada massa dui, a pretium odio cursus eu. Duis mattis lacinia leo in congue. Morbi pulvinar blandit sollicitudin. Morbi ultricies iaculis nisl non egestas. Praesent faucibus ut erat vel interdum. Donec et porttitor odio. Aenean sodales arcu sit amet diam pharetra, ut rhoncus urna lacinia.</P>

<P>Morbi eget velit sit amet ante blandit convallis. Praesent id congue arcu. Ut nulla ligula, pharetra ut sem vitae, pretium molestie felis. In ut consectetur turpis. Mauris rhoncus bibendum malesuada. Interdum et malesuada fames ac ante ipsum primis in faucibus. Vestibulum aliquet leo id sapien rutrum vulputate. Proin a tincidunt nibh. Donec lacus arcu, aliquam vel dui eu, finibus dictum justo. Vivamus eu sagittis tellus, quis tristique enim. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</P>'
WHERE projectID < 5000; 
  
SELECT * FROM project_tag WHERE projectID = 1;
  
SELECT * FROM placement;

INSERT INTO `placement` (`placementID`, `company_name`, `job_title`, `dates`, `job_description`, `company_website`, `screenshot_source`) VALUES
(1, 'Motor Design Ltd.', 'Software Developer', 'June 2012 - September 2012; June 2013 - August 2014; June 2015 - August 2015', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris sed dignissim massa. Phasellus id rhoncus eros. Morbi dignissim pharetra mi, vehicula interdum felis malesuada nec. Morbi id turpis odio. Cras rhoncus ante ut leo fringilla tincidunt. Phasellus lacinia urna vel odio gravida cursus. Duis ut volutpat metus, sit amet gravida metus. Etiam vitae tellus efficitur, ultrices sem vel, euismod ex. Nunc eget sollicitudin velit. Nullam quis dapibus magna. Nullam id tempor nulla.</p><p>Ut pharetra dapibus sapien, placerat porttitor nunc tincidunt sit amet. Duis tristique consectetur pulvinar. Suspendisse vitae varius nisi. Donec molestie placerat ex ac tempor. Mauris consectetur ex velit, ac dignissim sapien mattis id. Cras consequat mattis sapien a sodales. Proin consectetur maximus ante sed dictum. Duis lorem quam, rhoncus at sapien sit amet, tempus sagittis nibh.</p>', 'http://www.motor-design.com', 'Motor_Design_logo.png');

SELECT * FROM menu_section;
SELECT * FROM project;


ALTER TABLE project ADD menu_order INT(2);
ALTER TABLE project DROP menu_thumbnail_source;

SET PASSWORD FOR root@`::1` = PASSWORD('Punto21012011');

MDL:

<p>Joined in the summer of first year at university and continued to work during university
breaks up until graduation. This included a year-long industrial placement.</p>
<p>Learned and utilised advanced skills by programming in Object Pascal using
Delphi, creating various features for a software package by interpreting physics and
mathematical equations.</p>
<p>Created conductor placement and magnetic winding design tools for Motor-CAD.</p>
<p>Produced an interactive editor for solver circuit design also for Motor-CAD.</p>
<p>Also carried out projects using different methods of programming such as
VBA and PHP.</p>
<p>Greatly developed analytical skills and programming ability, as well as encouraging
working using my own initiative.</p>
<p>Subsequent to my placement year Motor Design Ltd. nominated me for the Placement
Student of the Year award.</p>

<ul class="text-left">
<li>Joined in the summer of first year at university and continued to work during university
breaks up until graduation. This included a year-long industrial placement.</li>
<li>Learned and utilised advanced skills by programming in Object Pascal using
Delphi, creating various features for a software package by interpreting physics and
mathematical equations.</li>
<li>Created conductor placement and magnetic winding design tools for Motor-CAD.</li>
<li>Produced an interactive editor for solver circuit design also for Motor-CAD.</li>
<li>Also carried out projects using different methods of programming such as
VBA and PHP.</li>
<li>Greatly developed analytical skills and programming ability, as well as encouraging
working using my own initiative.</li>
<li>Subsequent to my placement year Motor Design Ltd. nominated me for the Placement
Student of the Year award.</li>
</ul>

TAG:

<p>Received technical training in many different areas, including SQL, JavaScript, HTML
and CSS.</p>
<p>Received training and gained good experience using agile methodologies such as
Scrum and Kanban.</p>
<p>Carried out a project adding a new module to their charity donation software, thankQ.
This involved using the companies own SDK, as well as using Microsoft SQL Server and JavaScript.</p>

<ul class="text-left">
<li>Received technical training in many different areas, including SQL, JavaScript, HTML
and CSS.</li>
<li>Received training and gained good experience using agile methodologies such as
Scrum and Kanban.</li>
<li>Carried out a project adding a new module to their charity donation software, thankQ.
This involved using the companies own SDK, as well as using Microsoft SQL Server and JavaScript.</li>
</ul>

IBM:

<p>Currently on an IBM project taking the role of a Java developer for a large client within the public sector.</p>

<p>Previously undertook a role as part of a Tririga support team.</p>

<p>Was part of a small team which worked on an internal project developing an
Operations Portal for the Leicester CIC. This was developed in Java using the Spring
Framework.</p>
<p>Attended a 3 week IBM Tririga training course which was held in Virginia, US.</p>
<p>Have carried out a number of training courses.</p>

<ul class="text-left">
<li>Currently on an IBM project taking the role of a Java developer for a large client within the public sector.</li>
<li>Previously undertook a role as part of a Tririga support team.</li>

<li>Was part of a small team which worked on an internal project developing an
Operations Portal for the Leicester CIC. This was developed in Java using the Spring
Framework.</li>
<li>Attended a 3 week IBM Tririga training course which was held in Virginia, US.</li>
<li>Have carried out a number of training courses.</li>
</ul>

PY:

<p>After working for Motor Design Ltd during university breaks over summer, Christmas and Easter, I was subsequently given the opportunity to carry out an industrial placement, which would count towards my degree.</p>
<p>The placement year ran from June 2013 until August 2014. During the year I mainly continued in the same areas as I had been previously working in. This involved developing their main software package, Motor-CAD using the Object Pascal programming language. This included creating interactive graphical editors, as well as interpreting physics and mathematical equations. Towards the end of my placement I was also set a task using PHP, a language I had not had any previous experience with, however I was able to carry out the task required.</p>
<p>Throughout the year there was a noticeable improvement in my ability, whilst also giving me an experience of a professional working environment. This meant I was very well prepared for my final year of university and future career. This also lead to Motor Design Ltd. nominated me for the Placement Student of the Year award and an offer to return to work following my graduation.</p>
<p>As part of the assessment, my university required me to write a final report for my time during my industrial placement. The intention of this was to show what sort of tasks I had done during the year and what skills I had learned and developed.</p>


Projects:

Asteroids.exe
Asteroids Source code

Beetle Dodger.exe
Beetle Dodger Source code

Game Engine.exe
Game Engine source code

Monster Attack.apk
Monster Attack source code

Portfolio github
Portfolio link

PYCR.exe
PYCR source code

Euro 2016 Sweepstakes source code
Euro 2016 Sweepstakes link

Tank War.exe
Tank War source code

Smart Home assignment

Berties Buses assignment
Berties Buses source code

Football League assignment
Football League source code

FYP github
FYP final report

Premier Predictor github
Premier Predictor link

TO DO:

get data for projects
write project descriptions
do linkedIn
take better photo
publish