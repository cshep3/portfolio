-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 09, 2016 at 09:24 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `portfolio_db`
--
CREATE DATABASE IF NOT EXISTS `portfolio_db` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `portfolio_db`;

CREATE USER 'portfoliouser'@'localhost' IDENTIFIED BY 'portfolio_pass123';
GRANT SELECT ON `portfolio_db`.* TO 'portfoliouser'@'localhost';

-- --------------------------------------------------------

--
-- Table structure for table `job`
--

CREATE TABLE `job` (
  `jobID` int(10) UNSIGNED NOT NULL,
  `company_name` varchar(20) NOT NULL,
  `menu_section` int(10) UNSIGNED NOT NULL,
  `menu_thumbnail_source` varchar(255) NOT NULL,
  `job_title` varchar(100) DEFAULT NULL,
  `dates` varchar(100) DEFAULT NULL,
  `job_description` text,
  `company_website` varchar(30) DEFAULT NULL,
  `company_logo_source` varchar(60) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `job`
--

INSERT INTO `job` (`jobID`, `company_name`, `menu_section`, `menu_thumbnail_source`, `job_title`, `dates`, `job_description`, `company_website`, `company_logo_source`) VALUES
(1, 'Motor Design Ltd.', 0, '', 'Software Developer', 'June 2012 - September 2012; June 2013 - August 2014; June 2015 - August 2015', '<p>Joined in the summer of first year at university and continued to work during university\nbreaks up until graduation. This included a year-long industrial placement.</p>\n<p>Learned and utilised advanced skills by programming in Object Pascal using\nDelphi, creating various features for a software package by interpreting physics and\nmathematical equations.</p>\n<p>Created conductor placement and magnetic winding design tools for Motor-CAD.</p>\n<p>Produced an interactive editor for solver circuit design also for Motor-CAD.</p>\n<p>Added functionality for a cuboidal winding slot model within Motor-CAD.</p>\n<p>Greatly developed analytical skills and programming ability, as well as encouraging\nworking using my own initiative.</p>\n<p>Subsequent to my placement year Motor Design Ltd. nominated me for the Placement\nStudent of the Year award.</p>', 'http://www.motor-design.com', 'Motor_Design_logo.png'),
(2, 'The Access Group', 0, '', 'Junior Application Developer', 'August 2015 - October 2015', '<p>Received technical training in many different areas, including SQL, JavaScript, HTML\nand CSS.</p>\n<p>Received training and gained good experience using agile methodologies such as\nScrum and Kanban.</p>\n<p>Carried out a project adding a new module to their charity donation software, thankQ.\nThis involved using the companies own SDK, as well as using Microsoft SQL Server and JavaScript.</p>', 'http://www.theaccessgroup.com', 'The_Access_Group_logo.png'),
(3, 'IBM UK', 0, '', 'Developer', 'October 2015 - Present', '<p>Currently on an IBM project taking the role of a Java developer for a large client within the public sector.</p>\n\n<p>Previously undertook a role as part of a Tririga support team.</p>\n\n<p>Was part of a small team which worked on an internal project developing an\nOperations Portal for the Leicester CIC. This was developed in Java using the Spring\nFramework.</p>\n<p>Attended a 3 week IBM Tririga training course which was held in Virginia, US.</p>\n<p>Have carried out a number of training courses.</p>', 'http://www.ibm.com/uk-en/', 'IBM_logo.png');

-- --------------------------------------------------------

--
-- Table structure for table `job_tag`
--

CREATE TABLE `job_tag` (
  `tagID` int(10) UNSIGNED NOT NULL,
  `jobID` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `menu_section`
--

CREATE TABLE `menu_section` (
  `menu_sectionID` int(10) UNSIGNED NOT NULL,
  `menu_section_title` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `menu_section`
--

INSERT INTO `menu_section` (`menu_sectionID`, `menu_section_title`) VALUES
(1, 'C++'),
(2, 'Java'),
(3, 'PHP'),
(4, 'Matlab'),
(5, 'Visual Basic'),
(6, 'Industrial Placement'),
(7, 'Employment History');

-- --------------------------------------------------------

--
-- Table structure for table `placement`
--

CREATE TABLE `placement` (
  `placementID` int(10) UNSIGNED NOT NULL,
  `company_name` varchar(20) NOT NULL,
  `job_title` varchar(20) NOT NULL,
  `dates` varchar(100) DEFAULT NULL,
  `job_description` text NOT NULL,
  `company_website` varchar(30) DEFAULT NULL,
  `download_source` varchar(255) NOT NULL,
  `screenshot_source` varchar(60) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `placement`
--

INSERT INTO `placement` (`placementID`, `company_name`, `job_title`, `dates`, `job_description`, `company_website`, `download_source`, `screenshot_source`) VALUES
(1, 'Motor Design Ltd.', 'Software Developer', 'June 2013 - August 2014', '<p>After working for Motor Design Ltd during university breaks over summer, Christmas and Easter, I was subsequently given the opportunity to carry out an industrial placement, which would count towards my degree.</p>\n<p>The placement year ran from June 2013 until August 2014. During the year I mainly continued in the same areas as I had been previously working in. This involved developing their main software package, Motor-CAD using the Object Pascal programming language. This included creating interactive graphical editors, as well as interpreting physics and mathematical equations. Towards the end of my placement I was also set a task using PHP, a language I had not had any previous experience with, however I was able to carry out the task required.</p>\n<p>Throughout the year there was a noticeable improvement in my ability, whilst also giving me an experience of a professional working environment. This meant I was very well prepared for my final year of university and future career. This also lead to Motor Design Ltd. nominated me for the Placement Student of the Year award and an offer to return to work following my graduation.</p>\n<p>As part of the assessment, my university required me to write a final report for my time during my industrial placement. The intention of this was to show what sort of tasks I had done during the year and what skills I had learned and developed.</p>', 'http://www.motor-design.com', 'Written_Assignment.pdf', 'Motor_Design_logo.png');

-- --------------------------------------------------------

--
-- Table structure for table `placement_tag`
--

CREATE TABLE `placement_tag` (
  `tagID` int(10) UNSIGNED NOT NULL,
  `placementID` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `portfolio_error_log`
--

CREATE TABLE `portfolio_error_log` (
  `logID` int(10) UNSIGNED NOT NULL,
  `log_message` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `log_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `project`
--

CREATE TABLE `project` (
  `projectID` int(10) UNSIGNED NOT NULL,
  `project_title` varchar(100) DEFAULT NULL,
  `screenshot_source` varchar(255) NOT NULL,
  `menu_section` int(10) UNSIGNED NOT NULL,
  `menu_title` varchar(100) DEFAULT NULL,
  `project_description` text NOT NULL,
  `project_please_note` text NOT NULL,
  `project_download1_text` varchar(100) DEFAULT NULL,
  `project_download1_source` varchar(255) NOT NULL,
  `project_download2_text` varchar(100) DEFAULT NULL,
  `project_download2_source` varchar(255) NOT NULL,
  `menu_order` int(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `project`
--

INSERT INTO `project` (`projectID`, `project_title`, `screenshot_source`, `menu_section`, `menu_title`, `project_description`, `project_please_note`, `project_download1_text`, `project_download1_source`, `project_download2_text`, `project_download2_source`, `menu_order`) VALUES
(1, 'Asteroids', 'Asteroids.png', 1, 'Asteroids', '<P>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus hendrerit pulvinar lectus quis suscipit. Nunc sodales ac sem eu accumsan. Quisque at rhoncus felis, in dignissim justo. Vestibulum vitae tellus venenatis, venenatis ante sed, varius libero. Donec at arcu lobortis purus commodo ullamcorper vitae non nisl. Pellentesque bibendum id dolor et tincidunt. Cras erat felis, posuere malesuada efficitur ultricies, volutpat ut erat. Fusce vel felis vel tortor accumsan volutpat pulvinar a dui. Cras sem velit, viverra eget porttitor non, molestie quis erat. Morbi lacinia quis diam eget porta. Nulla venenatis rhoncus tellus nec blandit. Vivamus sit amet interdum sem, non bibendum odio.</P>\n\n<P>Ut viverra lorem a hendrerit feugiat. Proin vel pharetra ex. Integer auctor nisi ut bibendum eleifend. Pellentesque bibendum nunc feugiat odio egestas volutpat. Ut eu tortor dui. Donec iaculis nisi vel nisl tristique pulvinar. Maecenas iaculis leo eu gravida elementum. Nunc justo mauris, cursus sit amet volutpat quis, facilisis ac magna. Phasellus viverra vehicula enim, quis faucibus tellus porta eu. Fusce malesuada massa dui, a pretium odio cursus eu. Duis mattis lacinia leo in congue. Morbi pulvinar blandit sollicitudin. Morbi ultricies iaculis nisl non egestas. Praesent faucibus ut erat vel interdum. Donec et porttitor odio. Aenean sodales arcu sit amet diam pharetra, ut rhoncus urna lacinia.</P>\n\n<P>Morbi eget velit sit amet ante blandit convallis. Praesent id congue arcu. Ut nulla ligula, pharetra ut sem vitae, pretium molestie felis. In ut consectetur turpis. Mauris rhoncus bibendum malesuada. Interdum et malesuada fames ac ante ipsum primis in faucibus. Vestibulum aliquet leo id sapien rutrum vulputate. Proin a tincidunt nibh. Donec lacus arcu, aliquam vel dui eu, finibus dictum justo. Vivamus eu sagittis tellus, quis tristique enim. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</P>', 'University 1st year assignment to create the arcade game "Asteroids"', 'Download with Source Code', '', 'Download without Source Code', '', 2),
(2, 'Beetle Dodger', 'Beetle_Dodger.png', 1, 'Beetle Dodger', '<P>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus hendrerit pulvinar lectus quis suscipit. Nunc sodales ac sem eu accumsan. Quisque at rhoncus felis, in dignissim justo. Vestibulum vitae tellus venenatis, venenatis ante sed, varius libero. Donec at arcu lobortis purus commodo ullamcorper vitae non nisl. Pellentesque bibendum id dolor et tincidunt. Cras erat felis, posuere malesuada efficitur ultricies, volutpat ut erat. Fusce vel felis vel tortor accumsan volutpat pulvinar a dui. Cras sem velit, viverra eget porttitor non, molestie quis erat. Morbi lacinia quis diam eget porta. Nulla venenatis rhoncus tellus nec blandit. Vivamus sit amet interdum sem, non bibendum odio.</P>\n\n<P>Ut viverra lorem a hendrerit feugiat. Proin vel pharetra ex. Integer auctor nisi ut bibendum eleifend. Pellentesque bibendum nunc feugiat odio egestas volutpat. Ut eu tortor dui. Donec iaculis nisi vel nisl tristique pulvinar. Maecenas iaculis leo eu gravida elementum. Nunc justo mauris, cursus sit amet volutpat quis, facilisis ac magna. Phasellus viverra vehicula enim, quis faucibus tellus porta eu. Fusce malesuada massa dui, a pretium odio cursus eu. Duis mattis lacinia leo in congue. Morbi pulvinar blandit sollicitudin. Morbi ultricies iaculis nisl non egestas. Praesent faucibus ut erat vel interdum. Donec et porttitor odio. Aenean sodales arcu sit amet diam pharetra, ut rhoncus urna lacinia.</P>\n\n<P>Morbi eget velit sit amet ante blandit convallis. Praesent id congue arcu. Ut nulla ligula, pharetra ut sem vitae, pretium molestie felis. In ut consectetur turpis. Mauris rhoncus bibendum malesuada. Interdum et malesuada fames ac ante ipsum primis in faucibus. Vestibulum aliquet leo id sapien rutrum vulputate. Proin a tincidunt nibh. Donec lacus arcu, aliquam vel dui eu, finibus dictum justo. Vivamus eu sagittis tellus, quis tristique enim. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</P>', 'University 2st year assignment to create a 2D platform game', 'Download with Source Code', '', 'Download without Source Code', '', 3),
(3, '3D Game Engine', 'Game_Engine.png', 1, '3D Game Engine', '<P>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus hendrerit pulvinar lectus quis suscipit. Nunc sodales ac sem eu accumsan. Quisque at rhoncus felis, in dignissim justo. Vestibulum vitae tellus venenatis, venenatis ante sed, varius libero. Donec at arcu lobortis purus commodo ullamcorper vitae non nisl. Pellentesque bibendum id dolor et tincidunt. Cras erat felis, posuere malesuada efficitur ultricies, volutpat ut erat. Fusce vel felis vel tortor accumsan volutpat pulvinar a dui. Cras sem velit, viverra eget porttitor non, molestie quis erat. Morbi lacinia quis diam eget porta. Nulla venenatis rhoncus tellus nec blandit. Vivamus sit amet interdum sem, non bibendum odio.</P>\n\n<P>Ut viverra lorem a hendrerit feugiat. Proin vel pharetra ex. Integer auctor nisi ut bibendum eleifend. Pellentesque bibendum nunc feugiat odio egestas volutpat. Ut eu tortor dui. Donec iaculis nisi vel nisl tristique pulvinar. Maecenas iaculis leo eu gravida elementum. Nunc justo mauris, cursus sit amet volutpat quis, facilisis ac magna. Phasellus viverra vehicula enim, quis faucibus tellus porta eu. Fusce malesuada massa dui, a pretium odio cursus eu. Duis mattis lacinia leo in congue. Morbi pulvinar blandit sollicitudin. Morbi ultricies iaculis nisl non egestas. Praesent faucibus ut erat vel interdum. Donec et porttitor odio. Aenean sodales arcu sit amet diam pharetra, ut rhoncus urna lacinia.</P>\n\n<P>Morbi eget velit sit amet ante blandit convallis. Praesent id congue arcu. Ut nulla ligula, pharetra ut sem vitae, pretium molestie felis. In ut consectetur turpis. Mauris rhoncus bibendum malesuada. Interdum et malesuada fames ac ante ipsum primis in faucibus. Vestibulum aliquet leo id sapien rutrum vulputate. Proin a tincidunt nibh. Donec lacus arcu, aliquam vel dui eu, finibus dictum justo. Vivamus eu sagittis tellus, quis tristique enim. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</P>', 'University 3rd year assignment to create a 3D game engine', 'Download with Source Code', '', 'Download without Source Code', '', 5),
(4, 'Monster Attack', 'Monster_Attack.png', 2, 'Monster Attack', '<P>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus hendrerit pulvinar lectus quis suscipit. Nunc sodales ac sem eu accumsan. Quisque at rhoncus felis, in dignissim justo. Vestibulum vitae tellus venenatis, venenatis ante sed, varius libero. Donec at arcu lobortis purus commodo ullamcorper vitae non nisl. Pellentesque bibendum id dolor et tincidunt. Cras erat felis, posuere malesuada efficitur ultricies, volutpat ut erat. Fusce vel felis vel tortor accumsan volutpat pulvinar a dui. Cras sem velit, viverra eget porttitor non, molestie quis erat. Morbi lacinia quis diam eget porta. Nulla venenatis rhoncus tellus nec blandit. Vivamus sit amet interdum sem, non bibendum odio.</P>\n\n<P>Ut viverra lorem a hendrerit feugiat. Proin vel pharetra ex. Integer auctor nisi ut bibendum eleifend. Pellentesque bibendum nunc feugiat odio egestas volutpat. Ut eu tortor dui. Donec iaculis nisi vel nisl tristique pulvinar. Maecenas iaculis leo eu gravida elementum. Nunc justo mauris, cursus sit amet volutpat quis, facilisis ac magna. Phasellus viverra vehicula enim, quis faucibus tellus porta eu. Fusce malesuada massa dui, a pretium odio cursus eu. Duis mattis lacinia leo in congue. Morbi pulvinar blandit sollicitudin. Morbi ultricies iaculis nisl non egestas. Praesent faucibus ut erat vel interdum. Donec et porttitor odio. Aenean sodales arcu sit amet diam pharetra, ut rhoncus urna lacinia.</P>\n\n<P>Morbi eget velit sit amet ante blandit convallis. Praesent id congue arcu. Ut nulla ligula, pharetra ut sem vitae, pretium molestie felis. In ut consectetur turpis. Mauris rhoncus bibendum malesuada. Interdum et malesuada fames ac ante ipsum primis in faucibus. Vestibulum aliquet leo id sapien rutrum vulputate. Proin a tincidunt nibh. Donec lacus arcu, aliquam vel dui eu, finibus dictum justo. Vivamus eu sagittis tellus, quis tristique enim. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</P>', 'University 3rd year assignment to create a mobile game', 'Download with Source Code', '', 'Download .apk File', '', 6),
(5, 'Portfolio', 'Portfolio.png', 3, 'Portfolio', '<P>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus hendrerit pulvinar lectus quis suscipit. Nunc sodales ac sem eu accumsan. Quisque at rhoncus felis, in dignissim justo. Vestibulum vitae tellus venenatis, venenatis ante sed, varius libero. Donec at arcu lobortis purus commodo ullamcorper vitae non nisl. Pellentesque bibendum id dolor et tincidunt. Cras erat felis, posuere malesuada efficitur ultricies, volutpat ut erat. Fusce vel felis vel tortor accumsan volutpat pulvinar a dui. Cras sem velit, viverra eget porttitor non, molestie quis erat. Morbi lacinia quis diam eget porta. Nulla venenatis rhoncus tellus nec blandit. Vivamus sit amet interdum sem, non bibendum odio.</P>\n\n<P>Ut viverra lorem a hendrerit feugiat. Proin vel pharetra ex. Integer auctor nisi ut bibendum eleifend. Pellentesque bibendum nunc feugiat odio egestas volutpat. Ut eu tortor dui. Donec iaculis nisi vel nisl tristique pulvinar. Maecenas iaculis leo eu gravida elementum. Nunc justo mauris, cursus sit amet volutpat quis, facilisis ac magna. Phasellus viverra vehicula enim, quis faucibus tellus porta eu. Fusce malesuada massa dui, a pretium odio cursus eu. Duis mattis lacinia leo in congue. Morbi pulvinar blandit sollicitudin. Morbi ultricies iaculis nisl non egestas. Praesent faucibus ut erat vel interdum. Donec et porttitor odio. Aenean sodales arcu sit amet diam pharetra, ut rhoncus urna lacinia.</P>\n\n<P>Morbi eget velit sit amet ante blandit convallis. Praesent id congue arcu. Ut nulla ligula, pharetra ut sem vitae, pretium molestie felis. In ut consectetur turpis. Mauris rhoncus bibendum malesuada. Interdum et malesuada fames ac ante ipsum primis in faucibus. Vestibulum aliquet leo id sapien rutrum vulputate. Proin a tincidunt nibh. Donec lacus arcu, aliquam vel dui eu, finibus dictum justo. Vivamus eu sagittis tellus, quis tristique enim. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</P>', 'Personal project to create a portfolio website', 'Download Source Code', '', 'Portfolio Link', '', 10),
(6, 'Play Your Cards Right', 'PYCR.png', 1, 'Play Your Cards Right', '<P>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus hendrerit pulvinar lectus quis suscipit. Nunc sodales ac sem eu accumsan. Quisque at rhoncus felis, in dignissim justo. Vestibulum vitae tellus venenatis, venenatis ante sed, varius libero. Donec at arcu lobortis purus commodo ullamcorper vitae non nisl. Pellentesque bibendum id dolor et tincidunt. Cras erat felis, posuere malesuada efficitur ultricies, volutpat ut erat. Fusce vel felis vel tortor accumsan volutpat pulvinar a dui. Cras sem velit, viverra eget porttitor non, molestie quis erat. Morbi lacinia quis diam eget porta. Nulla venenatis rhoncus tellus nec blandit. Vivamus sit amet interdum sem, non bibendum odio.</P>\n\n<P>Ut viverra lorem a hendrerit feugiat. Proin vel pharetra ex. Integer auctor nisi ut bibendum eleifend. Pellentesque bibendum nunc feugiat odio egestas volutpat. Ut eu tortor dui. Donec iaculis nisi vel nisl tristique pulvinar. Maecenas iaculis leo eu gravida elementum. Nunc justo mauris, cursus sit amet volutpat quis, facilisis ac magna. Phasellus viverra vehicula enim, quis faucibus tellus porta eu. Fusce malesuada massa dui, a pretium odio cursus eu. Duis mattis lacinia leo in congue. Morbi pulvinar blandit sollicitudin. Morbi ultricies iaculis nisl non egestas. Praesent faucibus ut erat vel interdum. Donec et porttitor odio. Aenean sodales arcu sit amet diam pharetra, ut rhoncus urna lacinia.</P>\n\n<P>Morbi eget velit sit amet ante blandit convallis. Praesent id congue arcu. Ut nulla ligula, pharetra ut sem vitae, pretium molestie felis. In ut consectetur turpis. Mauris rhoncus bibendum malesuada. Interdum et malesuada fames ac ante ipsum primis in faucibus. Vestibulum aliquet leo id sapien rutrum vulputate. Proin a tincidunt nibh. Donec lacus arcu, aliquam vel dui eu, finibus dictum justo. Vivamus eu sagittis tellus, quis tristique enim. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</P>', 'University 1st year project to create a game based on "Play Your Cards Right"', 'Download with Source Code', '', 'Download without Source Code', '', 1),
(7, 'Euro 2016 Predictor', 'Euro_2016_Sweepstakes.png', 3, 'Euro 2016 Predictor', '<P>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus hendrerit pulvinar lectus quis suscipit. Nunc sodales ac sem eu accumsan. Quisque at rhoncus felis, in dignissim justo. Vestibulum vitae tellus venenatis, venenatis ante sed, varius libero. Donec at arcu lobortis purus commodo ullamcorper vitae non nisl. Pellentesque bibendum id dolor et tincidunt. Cras erat felis, posuere malesuada efficitur ultricies, volutpat ut erat. Fusce vel felis vel tortor accumsan volutpat pulvinar a dui. Cras sem velit, viverra eget porttitor non, molestie quis erat. Morbi lacinia quis diam eget porta. Nulla venenatis rhoncus tellus nec blandit. Vivamus sit amet interdum sem, non bibendum odio.</P>\n\n<P>Ut viverra lorem a hendrerit feugiat. Proin vel pharetra ex. Integer auctor nisi ut bibendum eleifend. Pellentesque bibendum nunc feugiat odio egestas volutpat. Ut eu tortor dui. Donec iaculis nisi vel nisl tristique pulvinar. Maecenas iaculis leo eu gravida elementum. Nunc justo mauris, cursus sit amet volutpat quis, facilisis ac magna. Phasellus viverra vehicula enim, quis faucibus tellus porta eu. Fusce malesuada massa dui, a pretium odio cursus eu. Duis mattis lacinia leo in congue. Morbi pulvinar blandit sollicitudin. Morbi ultricies iaculis nisl non egestas. Praesent faucibus ut erat vel interdum. Donec et porttitor odio. Aenean sodales arcu sit amet diam pharetra, ut rhoncus urna lacinia.</P>\n\n<P>Morbi eget velit sit amet ante blandit convallis. Praesent id congue arcu. Ut nulla ligula, pharetra ut sem vitae, pretium molestie felis. In ut consectetur turpis. Mauris rhoncus bibendum malesuada. Interdum et malesuada fames ac ante ipsum primis in faucibus. Vestibulum aliquet leo id sapien rutrum vulputate. Proin a tincidunt nibh. Donec lacus arcu, aliquam vel dui eu, finibus dictum justo. Vivamus eu sagittis tellus, quis tristique enim. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</P>', 'Personal project to create a predictor website for the 2016 Football European Championships', 'Download Source Code', '', 'Predictor Link', 'https://sweepstakes-cshep4.c9users.io/', 9),
(8, 'Tank War', 'Tank_War.png', 1, 'Tank War', '<P>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus hendrerit pulvinar lectus quis suscipit. Nunc sodales ac sem eu accumsan. Quisque at rhoncus felis, in dignissim justo. Vestibulum vitae tellus venenatis, venenatis ante sed, varius libero. Donec at arcu lobortis purus commodo ullamcorper vitae non nisl. Pellentesque bibendum id dolor et tincidunt. Cras erat felis, posuere malesuada efficitur ultricies, volutpat ut erat. Fusce vel felis vel tortor accumsan volutpat pulvinar a dui. Cras sem velit, viverra eget porttitor non, molestie quis erat. Morbi lacinia quis diam eget porta. Nulla venenatis rhoncus tellus nec blandit. Vivamus sit amet interdum sem, non bibendum odio.</P>\n\n<P>Ut viverra lorem a hendrerit feugiat. Proin vel pharetra ex. Integer auctor nisi ut bibendum eleifend. Pellentesque bibendum nunc feugiat odio egestas volutpat. Ut eu tortor dui. Donec iaculis nisi vel nisl tristique pulvinar. Maecenas iaculis leo eu gravida elementum. Nunc justo mauris, cursus sit amet volutpat quis, facilisis ac magna. Phasellus viverra vehicula enim, quis faucibus tellus porta eu. Fusce malesuada massa dui, a pretium odio cursus eu. Duis mattis lacinia leo in congue. Morbi pulvinar blandit sollicitudin. Morbi ultricies iaculis nisl non egestas. Praesent faucibus ut erat vel interdum. Donec et porttitor odio. Aenean sodales arcu sit amet diam pharetra, ut rhoncus urna lacinia.</P>\n\n<P>Morbi eget velit sit amet ante blandit convallis. Praesent id congue arcu. Ut nulla ligula, pharetra ut sem vitae, pretium molestie felis. In ut consectetur turpis. Mauris rhoncus bibendum malesuada. Interdum et malesuada fames ac ante ipsum primis in faucibus. Vestibulum aliquet leo id sapien rutrum vulputate. Proin a tincidunt nibh. Donec lacus arcu, aliquam vel dui eu, finibus dictum justo. Vivamus eu sagittis tellus, quis tristique enim. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</P>', 'University 2nd year assignment to create the AI for a Tank War game', 'Download with Source Code', '', 'Download without Source Code', '', 4),
(9, 'Fuzzy Logic Smart Home', 'Smart_Home.png', 4, 'Smart Home', '<P>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus hendrerit pulvinar lectus quis suscipit. Nunc sodales ac sem eu accumsan. Quisque at rhoncus felis, in dignissim justo. Vestibulum vitae tellus venenatis, venenatis ante sed, varius libero. Donec at arcu lobortis purus commodo ullamcorper vitae non nisl. Pellentesque bibendum id dolor et tincidunt. Cras erat felis, posuere malesuada efficitur ultricies, volutpat ut erat. Fusce vel felis vel tortor accumsan volutpat pulvinar a dui. Cras sem velit, viverra eget porttitor non, molestie quis erat. Morbi lacinia quis diam eget porta. Nulla venenatis rhoncus tellus nec blandit. Vivamus sit amet interdum sem, non bibendum odio.</P>\n\n<P>Ut viverra lorem a hendrerit feugiat. Proin vel pharetra ex. Integer auctor nisi ut bibendum eleifend. Pellentesque bibendum nunc feugiat odio egestas volutpat. Ut eu tortor dui. Donec iaculis nisi vel nisl tristique pulvinar. Maecenas iaculis leo eu gravida elementum. Nunc justo mauris, cursus sit amet volutpat quis, facilisis ac magna. Phasellus viverra vehicula enim, quis faucibus tellus porta eu. Fusce malesuada massa dui, a pretium odio cursus eu. Duis mattis lacinia leo in congue. Morbi pulvinar blandit sollicitudin. Morbi ultricies iaculis nisl non egestas. Praesent faucibus ut erat vel interdum. Donec et porttitor odio. Aenean sodales arcu sit amet diam pharetra, ut rhoncus urna lacinia.</P>\n\n<P>Morbi eget velit sit amet ante blandit convallis. Praesent id congue arcu. Ut nulla ligula, pharetra ut sem vitae, pretium molestie felis. In ut consectetur turpis. Mauris rhoncus bibendum malesuada. Interdum et malesuada fames ac ante ipsum primis in faucibus. Vestibulum aliquet leo id sapien rutrum vulputate. Proin a tincidunt nibh. Donec lacus arcu, aliquam vel dui eu, finibus dictum justo. Vivamus eu sagittis tellus, quis tristique enim. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</P>', 'University 3rd year assignment to create a Knowledge Based System for a Smart Home', 'Download Assignment', '', NULL, '', 11),
(10, 'Bertie''s Buses', 'Berties_Buses.png', 5, 'Bertie''s Buses', '<P>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus hendrerit pulvinar lectus quis suscipit. Nunc sodales ac sem eu accumsan. Quisque at rhoncus felis, in dignissim justo. Vestibulum vitae tellus venenatis, venenatis ante sed, varius libero. Donec at arcu lobortis purus commodo ullamcorper vitae non nisl. Pellentesque bibendum id dolor et tincidunt. Cras erat felis, posuere malesuada efficitur ultricies, volutpat ut erat. Fusce vel felis vel tortor accumsan volutpat pulvinar a dui. Cras sem velit, viverra eget porttitor non, molestie quis erat. Morbi lacinia quis diam eget porta. Nulla venenatis rhoncus tellus nec blandit. Vivamus sit amet interdum sem, non bibendum odio.</P>\n\n<P>Ut viverra lorem a hendrerit feugiat. Proin vel pharetra ex. Integer auctor nisi ut bibendum eleifend. Pellentesque bibendum nunc feugiat odio egestas volutpat. Ut eu tortor dui. Donec iaculis nisi vel nisl tristique pulvinar. Maecenas iaculis leo eu gravida elementum. Nunc justo mauris, cursus sit amet volutpat quis, facilisis ac magna. Phasellus viverra vehicula enim, quis faucibus tellus porta eu. Fusce malesuada massa dui, a pretium odio cursus eu. Duis mattis lacinia leo in congue. Morbi pulvinar blandit sollicitudin. Morbi ultricies iaculis nisl non egestas. Praesent faucibus ut erat vel interdum. Donec et porttitor odio. Aenean sodales arcu sit amet diam pharetra, ut rhoncus urna lacinia.</P>\n\n<P>Morbi eget velit sit amet ante blandit convallis. Praesent id congue arcu. Ut nulla ligula, pharetra ut sem vitae, pretium molestie felis. In ut consectetur turpis. Mauris rhoncus bibendum malesuada. Interdum et malesuada fames ac ante ipsum primis in faucibus. Vestibulum aliquet leo id sapien rutrum vulputate. Proin a tincidunt nibh. Donec lacus arcu, aliquam vel dui eu, finibus dictum justo. Vivamus eu sagittis tellus, quis tristique enim. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</P>', 'AS-Level assignment to create a coach booking system', 'Download Source Code', '', 'Download Assignment', '', 12),
(11, 'Football League System', 'Football_League.png', 5, 'Football League', '<P>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus hendrerit pulvinar lectus quis suscipit. Nunc sodales ac sem eu accumsan. Quisque at rhoncus felis, in dignissim justo. Vestibulum vitae tellus venenatis, venenatis ante sed, varius libero. Donec at arcu lobortis purus commodo ullamcorper vitae non nisl. Pellentesque bibendum id dolor et tincidunt. Cras erat felis, posuere malesuada efficitur ultricies, volutpat ut erat. Fusce vel felis vel tortor accumsan volutpat pulvinar a dui. Cras sem velit, viverra eget porttitor non, molestie quis erat. Morbi lacinia quis diam eget porta. Nulla venenatis rhoncus tellus nec blandit. Vivamus sit amet interdum sem, non bibendum odio.</P>\n\n<P>Ut viverra lorem a hendrerit feugiat. Proin vel pharetra ex. Integer auctor nisi ut bibendum eleifend. Pellentesque bibendum nunc feugiat odio egestas volutpat. Ut eu tortor dui. Donec iaculis nisi vel nisl tristique pulvinar. Maecenas iaculis leo eu gravida elementum. Nunc justo mauris, cursus sit amet volutpat quis, facilisis ac magna. Phasellus viverra vehicula enim, quis faucibus tellus porta eu. Fusce malesuada massa dui, a pretium odio cursus eu. Duis mattis lacinia leo in congue. Morbi pulvinar blandit sollicitudin. Morbi ultricies iaculis nisl non egestas. Praesent faucibus ut erat vel interdum. Donec et porttitor odio. Aenean sodales arcu sit amet diam pharetra, ut rhoncus urna lacinia.</P>\n\n<P>Morbi eget velit sit amet ante blandit convallis. Praesent id congue arcu. Ut nulla ligula, pharetra ut sem vitae, pretium molestie felis. In ut consectetur turpis. Mauris rhoncus bibendum malesuada. Interdum et malesuada fames ac ante ipsum primis in faucibus. Vestibulum aliquet leo id sapien rutrum vulputate. Proin a tincidunt nibh. Donec lacus arcu, aliquam vel dui eu, finibus dictum justo. Vivamus eu sagittis tellus, quis tristique enim. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</P>', 'A-Level assignment to create a football league system', 'Download Source Code', '', 'Download Assignment', '', 13),
(12, 'Final Year Project', 'Final_Year_Project.png', 3, 'Final Year Project', '<P>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus hendrerit pulvinar lectus quis suscipit. Nunc sodales ac sem eu accumsan. Quisque at rhoncus felis, in dignissim justo. Vestibulum vitae tellus venenatis, venenatis ante sed, varius libero. Donec at arcu lobortis purus commodo ullamcorper vitae non nisl. Pellentesque bibendum id dolor et tincidunt. Cras erat felis, posuere malesuada efficitur ultricies, volutpat ut erat. Fusce vel felis vel tortor accumsan volutpat pulvinar a dui. Cras sem velit, viverra eget porttitor non, molestie quis erat. Morbi lacinia quis diam eget porta. Nulla venenatis rhoncus tellus nec blandit. Vivamus sit amet interdum sem, non bibendum odio.</P>\n\n<P>Ut viverra lorem a hendrerit feugiat. Proin vel pharetra ex. Integer auctor nisi ut bibendum eleifend. Pellentesque bibendum nunc feugiat odio egestas volutpat. Ut eu tortor dui. Donec iaculis nisi vel nisl tristique pulvinar. Maecenas iaculis leo eu gravida elementum. Nunc justo mauris, cursus sit amet volutpat quis, facilisis ac magna. Phasellus viverra vehicula enim, quis faucibus tellus porta eu. Fusce malesuada massa dui, a pretium odio cursus eu. Duis mattis lacinia leo in congue. Morbi pulvinar blandit sollicitudin. Morbi ultricies iaculis nisl non egestas. Praesent faucibus ut erat vel interdum. Donec et porttitor odio. Aenean sodales arcu sit amet diam pharetra, ut rhoncus urna lacinia.</P>\n\n<P>Morbi eget velit sit amet ante blandit convallis. Praesent id congue arcu. Ut nulla ligula, pharetra ut sem vitae, pretium molestie felis. In ut consectetur turpis. Mauris rhoncus bibendum malesuada. Interdum et malesuada fames ac ante ipsum primis in faucibus. Vestibulum aliquet leo id sapien rutrum vulputate. Proin a tincidunt nibh. Donec lacus arcu, aliquam vel dui eu, finibus dictum justo. Vivamus eu sagittis tellus, quis tristique enim. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</P>', 'University Final Year Project to create a social media websiite', 'Download Source Code', '', 'Download Final Report', '', 8),
(13, 'M2M Connect', 'M2M_Connect.png', 3, 'M2M Connect', '<P>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus hendrerit pulvinar lectus quis suscipit. Nunc sodales ac sem eu accumsan. Quisque at rhoncus felis, in dignissim justo. Vestibulum vitae tellus venenatis, venenatis ante sed, varius libero. Donec at arcu lobortis purus commodo ullamcorper vitae non nisl. Pellentesque bibendum id dolor et tincidunt. Cras erat felis, posuere malesuada efficitur ultricies, volutpat ut erat. Fusce vel felis vel tortor accumsan volutpat pulvinar a dui. Cras sem velit, viverra eget porttitor non, molestie quis erat. Morbi lacinia quis diam eget porta. Nulla venenatis rhoncus tellus nec blandit. Vivamus sit amet interdum sem, non bibendum odio.</P>\n\n<P>Ut viverra lorem a hendrerit feugiat. Proin vel pharetra ex. Integer auctor nisi ut bibendum eleifend. Pellentesque bibendum nunc feugiat odio egestas volutpat. Ut eu tortor dui. Donec iaculis nisi vel nisl tristique pulvinar. Maecenas iaculis leo eu gravida elementum. Nunc justo mauris, cursus sit amet volutpat quis, facilisis ac magna. Phasellus viverra vehicula enim, quis faucibus tellus porta eu. Fusce malesuada massa dui, a pretium odio cursus eu. Duis mattis lacinia leo in congue. Morbi pulvinar blandit sollicitudin. Morbi ultricies iaculis nisl non egestas. Praesent faucibus ut erat vel interdum. Donec et porttitor odio. Aenean sodales arcu sit amet diam pharetra, ut rhoncus urna lacinia.</P>\n\n<P>Morbi eget velit sit amet ante blandit convallis. Praesent id congue arcu. Ut nulla ligula, pharetra ut sem vitae, pretium molestie felis. In ut consectetur turpis. Mauris rhoncus bibendum malesuada. Interdum et malesuada fames ac ante ipsum primis in faucibus. Vestibulum aliquet leo id sapien rutrum vulputate. Proin a tincidunt nibh. Donec lacus arcu, aliquam vel dui eu, finibus dictum justo. Vivamus eu sagittis tellus, quis tristique enim. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</P>', 'University 3rd year assignment to create a secure web application using a SOAP API', 'Download Source Code', '', NULL, '', 7);

-- --------------------------------------------------------

--
-- Table structure for table `project_tag`
--

CREATE TABLE `project_tag` (
  `tagID` int(10) UNSIGNED NOT NULL,
  `projectID` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `project_tag`
--

INSERT INTO `project_tag` (`tagID`, `projectID`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 6),
(2, 4),
(3, 5),
(6, 5),
(8, 5),
(9, 5),
(11, 1),
(11, 2),
(11, 6),
(12, 3),
(13, 4),
(14, 4),
(16, 1),
(16, 2),
(16, 3),
(16, 4),
(16, 5),
(16, 6),
(17, 5),
(21, 1),
(21, 2),
(21, 3),
(21, 4),
(21, 6),
(22, 1),
(22, 2),
(22, 4),
(22, 6),
(23, 3);

-- --------------------------------------------------------

--
-- Table structure for table `tag`
--

CREATE TABLE `tag` (
  `tagID` int(10) UNSIGNED NOT NULL,
  `tag` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tag`
--

INSERT INTO `tag` (`tagID`, `tag`) VALUES
(1, 'C++'),
(2, 'Java'),
(3, 'PHP'),
(4, 'Matlab'),
(5, 'Visual Basic'),
(6, 'JavaScript'),
(7, 'JQuery'),
(8, 'HTML'),
(9, 'CSS'),
(10, 'AJAX'),
(11, 'SFML'),
(12, 'OpenGL'),
(13, 'Android'),
(14, 'AI'),
(15, 'Web Services'),
(16, 'OO'),
(17, 'MVC Architecture'),
(18, 'Object Pascal'),
(19, 'Quicksort'),
(20, 'Fuzzy Logic'),
(21, 'Games'),
(22, '2D'),
(23, '3D'),
(24, 'A* Algorithm'),
(25, 'BDD'),
(26, 'TDD'),
(27, 'Encryption'),
(28, 'Hashing'),
(29, 'Captcha');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `job`
--
ALTER TABLE `job`
  ADD PRIMARY KEY (`jobID`);

--
-- Indexes for table `job_tag`
--
ALTER TABLE `job_tag`
  ADD PRIMARY KEY (`tagID`,`jobID`),
  ADD KEY `jobID` (`jobID`);

--
-- Indexes for table `menu_section`
--
ALTER TABLE `menu_section`
  ADD PRIMARY KEY (`menu_sectionID`);

--
-- Indexes for table `placement`
--
ALTER TABLE `placement`
  ADD PRIMARY KEY (`placementID`);

--
-- Indexes for table `placement_tag`
--
ALTER TABLE `placement_tag`
  ADD PRIMARY KEY (`tagID`,`placementID`),
  ADD KEY `placementID` (`placementID`);

--
-- Indexes for table `portfolio_error_log`
--
ALTER TABLE `portfolio_error_log`
  ADD PRIMARY KEY (`logID`);

--
-- Indexes for table `project`
--
ALTER TABLE `project`
  ADD PRIMARY KEY (`projectID`),
  ADD KEY `menu_section` (`menu_section`);

--
-- Indexes for table `project_tag`
--
ALTER TABLE `project_tag`
  ADD PRIMARY KEY (`tagID`,`projectID`),
  ADD KEY `projectID` (`projectID`);

--
-- Indexes for table `tag`
--
ALTER TABLE `tag`
  ADD PRIMARY KEY (`tagID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `job`
--
ALTER TABLE `job`
  MODIFY `jobID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `menu_section`
--
ALTER TABLE `menu_section`
  MODIFY `menu_sectionID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `placement`
--
ALTER TABLE `placement`
  MODIFY `placementID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `portfolio_error_log`
--
ALTER TABLE `portfolio_error_log`
  MODIFY `logID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `project`
--
ALTER TABLE `project`
  MODIFY `projectID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `tag`
--
ALTER TABLE `tag`
  MODIFY `tagID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `job_tag`
--
ALTER TABLE `job_tag`
  ADD CONSTRAINT `job_tag_ibfk_1` FOREIGN KEY (`tagID`) REFERENCES `tag` (`tagID`),
  ADD CONSTRAINT `job_tag_ibfk_2` FOREIGN KEY (`jobID`) REFERENCES `job` (`jobID`);

--
-- Constraints for table `placement_tag`
--
ALTER TABLE `placement_tag`
  ADD CONSTRAINT `placement_tag_ibfk_1` FOREIGN KEY (`tagID`) REFERENCES `tag` (`tagID`),
  ADD CONSTRAINT `placement_tag_ibfk_2` FOREIGN KEY (`placementID`) REFERENCES `placement` (`placementID`);

--
-- Constraints for table `project`
--
ALTER TABLE `project`
  ADD CONSTRAINT `project_ibfk_1` FOREIGN KEY (`menu_section`) REFERENCES `menu_section` (`menu_sectionID`);

--
-- Constraints for table `project_tag`
--
ALTER TABLE `project_tag`
  ADD CONSTRAINT `project_tag_ibfk_1` FOREIGN KEY (`tagID`) REFERENCES `tag` (`tagID`),
  ADD CONSTRAINT `project_tag_ibfk_2` FOREIGN KEY (`projectID`) REFERENCES `project` (`projectID`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
