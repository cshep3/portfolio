<?php
/**
 * class.PortfolioDisplayJobDetailsModel.php
 *
 * Portfolio website - Chris Shepherd
 *
 * @author Chris Shepherd - chris_shepherd2@hotmail.com
 *
 * @package portfolio
 */

class PortfolioDisplayJobDetailsModel extends PortfolioModelAbstract
{
    private $c_arr_job_details;
    private $c_sanitised_job_index;

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    public function __construct()
    {
        parent::__construct();
        $this->c_arr_job_details = array();
        $this->c_sanitised_job_index = '';
    }

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    public function get_job_details()
    {
        return $this->c_arr_job_details;
    }

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    public function set_job_index($p_arr_sanitised_input)
    {
        $m_sanitised_job_index = '';
        if (isset($p_arr_sanitised_input['index']))
        {
            $m_sanitised_job_index = $p_arr_sanitised_input['index'];
        }
        $this->c_sanitised_job_index = $m_sanitised_job_index;
    }

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    public function do_retrieve_job_details()
    {
        $m_sanitised_job_index = $this->c_sanitised_job_index;
        $m_sql_query_string = PortfolioSqlQuery::query_get_job_details();
        $m_arr_sql_query_parameters = array(':jobID' => $m_sanitised_job_index);
        $m_query_result = $this->c_obj_database_handle->safe_query($m_sql_query_string, $m_arr_sql_query_parameters);
        
        $m_job_count = $this->c_obj_database_handle->count_rows();
        if ($m_job_count == 0)
        {
            $m_sanitised_job_index = false;
        }
        else
        {
            $m_job_details = $this->c_obj_database_handle->safe_fetch_array();
            $this->c_arr_job_details['sanitised-job-index'] = $m_sanitised_job_index;
            $this->c_arr_job_details['job-details'] = $m_job_details;
        }
    }
    
    public function do_retrieve_job_tags()
    {
        $m_sanitised_job_index = $this->c_sanitised_job_index;
        $m_sql_query_string = PortfolioSqlQuery::query_get_job_tags();
        $m_arr_sql_query_parameters = array(':jobID' => $m_sanitised_job_index);
        
        $m_query_result = $this->c_obj_database_handle->safe_query($m_sql_query_string, $m_arr_sql_query_parameters);

        $m_tag_count = $this->c_obj_database_handle->count_rows();

        if ($m_tag_count == 0)
        {
            $this->c_arr_job_details['job-tags'] = null;
        }
        else
        {
            $this->c_arr_job_details['job-tags'] = $this->c_obj_database_handle->safe_fetch_all_results();
        }
    }
}
?>
