<?php
/**
 * class.PortfolioDisplayJobDetailsController.php
 *
 * Portfolio website - Chris Shepherd
 *
 * @author Chris Shepherd - chris_shepherd2@hotmail.com
 *
 * @package portfolio
 */

class PortfolioDisplayJobDetailsController extends PortfolioControllerAbstract
{

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    public function do_create_html_output()
    {
        $m_arr_sanitised_input = array();
        $m_arr_sanitised_input = $this->c_obj_container->make_portfolio_display_job_details_validate();

        if (isset($m_arr_sanitised_input['index']))
        {
            $m_arr_job_details = $this->c_obj_container->make_portfolio_display_job_details_model($m_arr_sanitised_input);
        }
        else
        {
            $m_arr_job_details = $m_arr_sanitised_input;
        }
        $this->c_html_output = $this->c_obj_container->make_portfolio_display_job_details_view($m_arr_job_details);
    }
}
?>
