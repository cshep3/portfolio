<?php
/**
 * class.PortfolioDisplayJobDetailsView.php
 *
 * Portfolio website - Chris Shepherd
 *
 * @author Chris Shepherd - chris_shepherd2@hotmail.com
 *
 * @package portfolio
 */

class PortfolioDisplayJobDetailsView extends PortfolioWebPageTemplateView
{
    private $c_arr_job_details;

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    public function __construct()
    {
        $this->c_arr_job_details = array();
    }

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    public function set_job_details($p_arr_job_details)
    {
        $this->c_arr_job_details = $p_arr_job_details;
    }

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    protected function do_assign_page_title()
    {
        if (isset($this->c_arr_job_details['sanitised-job-index'])) {
            if (!$this->c_arr_job_details['sanitised-job-index']) {
                $this->c_page_title = "Error";
            } else {
                $m_title = $this->c_arr_job_details['job-details']['company_name'];
                $this->c_page_title = $m_title;
            }
        } else {
            $this->c_page_title = "Error";
        }
    }
// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    protected function do_create_relevant_output()
    {
        if (isset($this->c_arr_job_details['sanitised-job-index']))
        {
            if (!$this->c_arr_job_details['sanitised-job-index'])
            {
                $this->do_create_error_message();
            }
            else
            {
                $this->do_display_job_details();
            }
        } else {
            $this->do_create_error_message();
        }
    }

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    private function do_create_error_message()
    {
        $m_address = APP_ROOT_PATH;
        $this->c_html_page_content = <<< NAMEERRORPAGE
<div class="container text-center">
<div class="well">
<div class="row">
<h1>Error</h1>
<div class="col-sm-12">
<div class="well">
<h3 class="error">Oops - there was a problem with the job
you selected/entered - Please try again.</h3>
<p>If you think this could be a problem with the site please
contact: <a href="mailto:chris_shepherd2@hotmail.com">chris_shepherd2@hotmail.com</a>.</p>
<h4><a href=$m_address>Return to Home Page</a></h4>
</div>
</div>
</div>
</div>
</div>
NAMEERRORPAGE;
    }

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    private function do_display_job_details()
    {
        $m_address = APP_ROOT_PATH;

        $m_job_index = $this->c_arr_job_details['sanitised-job-index'];
        $m_arr_job_details = $this->c_arr_job_details['job-details'];
        $m_arr_job_tags = $this->c_arr_job_details['job-tags'];

        $m_company_name = $m_arr_job_details['company_name'];
        $m_image_name = $m_company_name . " Logo";
        $m_logo_source = COMPANYLOGOS_PATH . $m_arr_job_details['company_logo_source'];
        $m_job_title = $m_arr_job_details['job_title'];
        $m_dates = $m_arr_job_details['dates'];
        $m_description = $m_arr_job_details['job_description'];
        $m_company_website = $m_arr_job_details['company_website'];

        $this->c_html_page_content = <<< VIEWJOBDETAILS
<div class="container text-center">
<div class="well">
  <div class="row">
	<h1>$m_company_name</h1>
    <div class="col-sm-6 col-sm-push-6">
      <div class="well">
        <h4>Company Logo</h4>
        <img src=$m_logo_source class="img-responsive" alt=$m_image_name width=200>
      </div>
      <div class="well">
        <h4>Skills</h4>
        <p>
VIEWJOBDETAILS;
            if ($m_arr_job_tags != null)
            {
                $m_tag_colour = null;
                foreach ($m_arr_job_tags as $m_tag)
                {
                    $m_new_tag_colour = $this->get_tag_colour($m_tag_colour);
                    $m_tag_colour = $m_new_tag_colour;
                    $m_tag_name = $m_tag['tag'];
                    $m_tag_id = $m_tag['tagID'];
                    $this->c_html_page_content .= <<< TAG
                    <span class="label label-$m_tag_colour">$m_tag_name</span>
TAG;
                }
            }
            $this->c_html_page_content .= <<< VIEWJOBDETAILS
        </p>
      </div>
      <div class="well">
        <h4>Dates<br>
        <small>$m_dates</small>
		</h4>
      </div>
      <div class="well">
        <h4>Company Website<br>
        <small><a href=$m_company_website>$m_company_website</a></small>
		</h4>
      </div>
    </div>
	
	<div class="col-sm-6 col-sm-pull-6">
		<h3>$m_job_title</h3>
		$m_description
	</div>
  </div>
</div>
</div>
VIEWJOBDETAILS;
    }

    private function get_tag_colour($a_tag_colour)
    {
        switch ($a_tag_colour)
            {
                case 'default':
                    return 'primary';
                  break;
                case 'primary':
                    return 'success';
                  break;
                case 'success':
                    return 'info';
                  break;
                case 'info':
                    return 'warning';
                  break;
                case 'warning':
                    return 'danger';
                  break;
                case 'danger':
                    return 'default';
                  break;  
                default:
                    return 'default';
                  break;
            }        
    }
}
?>
