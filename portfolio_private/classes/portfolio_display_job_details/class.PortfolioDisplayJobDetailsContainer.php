<?php
/**
 * class.PortfolioDisplayJobDetailsContainer.php
 *
 * Portfolio website - Chris Shepherd
 *
 * @author Chris Shepherd - chris_shepherd2@hotmail.com
 *
 * @package portfolio
 */

  class PortfolioDisplayJobDetailsContainer extends PortfolioContainerAbstract
  {
// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    public function __construct($p_feature_name)
    {
      $m_directory_path = realpath(dirname(__FILE__)) . DIRSEP;
      parent::__construct($p_feature_name, $m_directory_path);
    }

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    public function make_portfolio_display_job_details_controller()
    {
        $m_obj_self = $this;
        $m_obj_display_job_details_controller = new PortfolioDisplayJobDetailsController($m_obj_self);
        $m_obj_display_job_details_controller->do_create_html_output();
        $m_html_output = $m_obj_display_job_details_controller->get_html_output();
        return $m_html_output;
    }

    public function make_portfolio_display_job_details_validate()
    {
        $m_obj_display_job_details_validate = new PortfolioDisplayJobDetailsValidate();
        $m_obj_display_job_details_validate->do_sanitise_input();
        $m_arr_sanitised_input = $m_obj_display_job_details_validate->get_sanitised_input();
        return $m_arr_sanitised_input;
    }

    public function make_portfolio_display_job_details_model($p_arr_sanitised_input)
    {
        $m_obj_display_job_details_model = new PortfolioDisplayJobDetailsModel();
        $m_obj_display_job_details_model->set_job_index($p_arr_sanitised_input);
        $m_obj_display_job_details_model->do_retrieve_job_details();
        $m_obj_display_job_details_model->do_retrieve_job_tags();
        $m_arr_job_details = $m_obj_display_job_details_model->get_job_details();
        return $m_arr_job_details;
    }
    
    public static function make_portfolio_display_job_details_view($p_arr_job_details)
    {
        $m_obj_display_job_details_view = new PortfolioDisplayJobDetailsView();
        $m_obj_display_job_details_view->set_job_details($p_arr_job_details);
        $m_obj_display_job_details_view->do_create_output_page();
        $m_html_output = $m_obj_display_job_details_view->get_html_output();
        return $m_html_output;
    }
  }
?>
