<?php
  /**
   * class.PortfolioModelAbstract.php
   *
   * Portfolio website - Chris Shepherd
   *
   * @author Chris Shepherd - chris_shepherd2@hotmail.com
   *
   * @package portfolio
   */

  abstract class PortfolioViewAbstract
  {
    public final function __destruct() {}

    public function do_create_output_page()
    {
        $this->do_assign_page_title();
        $this->do_create_relevant_output();
        $this->create_web_page();
    }

    public function get_html_output()
    {
        return $this->c_html_page_output;
    }
  }
?>
