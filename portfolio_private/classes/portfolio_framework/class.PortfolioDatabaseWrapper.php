<?php
  /**
   * class.PortfolioDatabaseWrapper.php
   *
   * Portfolio website - Chris Shepherd
   *
   * @author Chris Shepherd - chris_shepherd2@hotmail.com
   *
   * @package portfolio
   */

  class PortfolioDatabaseWrapper
  {
    // access is private for all class variables
    private $c_arr_database_connect_details;
    private $c_arr_database_connection_messages;
    private $c_obj_database_handle;
    private $c_obj_stmt;

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    public function __construct()
    {
      $this->c_arr_database_connect_details = array();
      $this->c_obj_database_handle = null;
      $this->c_obj_stmt = null;
      $this->c_arr_database_connection_messages = array();
    }

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    /**
     * ensure disconnection of all service handles
     */
    public function __destruct() { $this->c_obj_database_handle = null; }

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    public function set_connection_settings($p_arr_connection_settings)
    {
      $this->c_arr_database_connect_details = $p_arr_connection_settings;
    }

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    // connect to the required database
    // generate error messages on error
    public function do_connect_to_database()
    {
      $m_database_connection_error = false;
      $m_host_name = $this->c_arr_database_connect_details['host_name'];
      $m_user_name = $this->c_arr_database_connect_details['user_name'];
      $m_user_password = $this->c_arr_database_connect_details['user_password'];
      // attempt to connect to database server & specified database
      try
      {
        $this->c_obj_database_handle = new PDO($m_host_name, $m_user_name, $m_user_password);
        $this->c_arr_database_connection_messages['connection'] = 'Connected to the database.';
      }
      catch (PDOException $exception_object)
      {
        $this->c_arr_database_connection_messages['connection'] = 'Cannot connect to the database.';
        $m_database_connection_error = true;
        trigger_error($m_host_name);
      }
      $this->c_arr_database_connection_messages['database-connection-error'] = $m_database_connection_error;
    }

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    public function get_connection_messages()
    {
      return $this->c_arr_database_connection_messages;
    }

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    // using prepared queries - additional code by af - 31/12/07
    public function safe_query($p_query_string, $p_arr_params = null)
    {
      $m_database_query_execute_error = false;
      $m_query_string = $p_query_string;
      $m_arr_query_parameters = $p_arr_params;

      try
      {
        $m_temp = array();
        $this->c_obj_database_handle->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $this->c_obj_stmt = $this->c_obj_database_handle->prepare($m_query_string);

        // bind the parameters
        if (sizeof($m_arr_query_parameters) > 0)
        {
          foreach ($m_arr_query_parameters as $m_param_key => $m_param_value)
          {
            $m_temp[$m_param_key] = $m_param_value;
            $this->c_obj_stmt->bindParam($m_param_key, $m_temp[$m_param_key], PDO::PARAM_STR);
          }
        }
        // execute the query
        $m_execute_result = $this->c_obj_stmt->execute();
        $this->c_arr_database_connection_messages['execute-OK'] = $m_execute_result;
      }
      catch (PDOException $exception_object)
      {
        $m_error_message  = 'PDO Exception caught. ';
        $m_error_message .= 'Error with the database access. ';
        $m_error_message .= 'SQL query: ' . $m_query_string;
        $m_error_message .= 'Error: ' . print_r($this->c_obj_stmt->errorInfo(), true) . "\n";
        // NB would usually output to file for sysadmin attention
        $m_database_query_execute_error = true;
        $this->c_arr_database_connection_messages['sql-error'] = $m_error_message;
        $this->c_arr_database_connection_messages['pdo-error-code'] = $this->c_obj_stmt->errorInfo();
      }
      $this->c_arr_database_connection_messages['database-query-execute-error'] = $m_database_query_execute_error;
      return $this->c_arr_database_connection_messages;
    }

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    // count number of returned rows in the record set
    public function count_rows()
    {
      $m_num_rows = $this->c_obj_stmt->rowCount();
      return $m_num_rows;
    } // end of function count_rows()

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    // count number of returned fields in the record set
    public function count_fields($p_query_result)
    {
      $m_num_fields = $p_query_result->columnCount();
      return $m_num_fields;
    } // end of function count_fields()

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    // return the record set with field indices
    public function safe_fetch_row()
    {
      $m_record_set = $this->c_obj_stmt->fetch(PDO::FETCH_NUM);
      return $m_record_set;
    }

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    // return the record set with field names
    public function safe_fetch_array()
    {
      $m_row = $this->c_obj_stmt->fetch(PDO::FETCH_ASSOC);
      return $m_row;
    }

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    // return the record set as an object
    public function safe_fetch_all_results()
    {
      $m_row = $this->c_obj_stmt->fetchAll();
      return $m_row;
    }

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    // return the record set as an object
    public function safe_fetch_object()
    {
      $m_row = $this->c_obj_stmt->fetchObject();
      return $m_row;
    }

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    // get id of last inserted row (auto-increment field)
    public function last_inserted_id()
    {
      $m_sql_query = 'SELECT LAST_INSERT_ID()';

      $this->safe_query($m_sql_query);
      $m_arr_last_inserted_id = $this->safe_fetch_array();
      $m_last_inserted_id = $m_arr_last_inserted_id['LAST_INSERT_ID()'];
      return $m_last_inserted_id;
    }

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    /**
     * Dumps the information contained by a prepared statement directly on the output.
     * It will provide the SQL query in use, the number of parameters used (Params),
     * the list of parameters, with their name, type (paramtype) as an integer, their
     * key name or position, the value, and the position in the query (if this is
     * supported by the PDO driver, otherwise, it will be -1).
     *
     * @return $m_debug_dump_params
     */
    public function debug_dump_parameters()
    {
      $m_debug_dump_params = $this->c_obj_stmt->debugDumpParams();
      return $m_debug_dump_params;
    }
  }
?>
