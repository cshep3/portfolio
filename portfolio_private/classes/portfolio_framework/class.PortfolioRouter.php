<?php
  /**
   * class.PortfolioRouter.php
   *
   * Portfolio website - Chris Shepherd
   *
   * @author Chris Shepherd - chris_shepherd2@hotmail.com
   *
   * @package portfolio
   */

  class PortfolioRouter
  {
    private $c_feature_in;
    private $c_feature;
    private $c_html_output;

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    public function __construct()
    {
      $this->c_feature_in = '';
      $this->c_feature = '';
      $this->c_html_output = '';
    }

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    public function __destruct(){}

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    public function do_routing()
    {
      $this->set_feature_name();
      $this->map_feature_name();
      $this->router_class();
      PortfolioContainer::make_portfolio_process_output($this->c_html_output);
    }

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    public function get_html_output()
    {
      return $this->c_html_output;
    }

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    private function set_feature_name()
    {
      if (isset($_GET['feature']))
      {
        $m_feature_in = $_GET['feature'];
      }
      else
      {
        $m_feature_in = 'index';
      }

      $this->c_feature_in = $m_feature_in;
    }

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    private function map_feature_name()
    {
      $m_feature_exists = false;
      // map the passed module name to an internal application feature name
      $m_features = array(
        'index' => 'index',
        'display_project_details' => 'display-project-details',
        'display_job_details' => 'display-job-details',
        'display_placement_details' => 'display-placement-details',
        'display_jobs' => 'display-jobs',
        'display_projects' => 'display-projects',
        'display_project_tags' => 'display-project-tags'
      );

      if (array_key_exists($this->c_feature_in, $m_features))
      {
        $this->c_feature = $m_features[$this->c_feature_in];
        $m_feature_exists =  true;
      }
      else
      {
        $m_obj_sessions_error = PortfolioContainer::make_portfolio_error('feature-not-found-error'); // add error to database, display index
      }
      return $m_feature_exists;
    }

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    public function router_class()
    {
      switch ($this->c_feature)
      {
        case 'display-project-details':
          $this->do_call_constructor('DisplayProjectDetails', 'make_portfolio_display_project_details_controller');
          break;
        case 'display-job-details':
          $this->do_call_constructor('DisplayJobDetails', 'make_portfolio_display_job_details_controller');
          break;
        case 'display-placement-details':
          $this->do_call_constructor('DisplayPlacementDetails', 'make_portfolio_display_placement_details_controller');
          break;
        case 'display-jobs':
          $this->do_call_constructor('DisplayJobs', 'make_portfolio_display_jobs_controller');
          break;
        case 'display-projects':
          $this->do_call_constructor('DisplayProjects', 'make_portfolio_display_projects_controller');
          break;
        case 'display-project-tags':
          $this->do_call_constructor('DisplayProjectTags', 'make_portfolio_display_project_tags_controller');
          break; 
        default:
          $this->do_call_constructor('Index', 'make_portfolio_index_controller');
          break;
      }
    }
    
    // ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    private function do_call_constructor($p_feature_name, $p_method_name)
    {
        $m_html_output = '';
        $m_class_name = APP_NAME . $p_feature_name . 'Container';
        $m_obj_constructor = new $m_class_name($p_feature_name);
        $m_html_output = $m_obj_constructor->$p_method_name();
        $this->c_html_output = $m_html_output;
    }
  }
?>
