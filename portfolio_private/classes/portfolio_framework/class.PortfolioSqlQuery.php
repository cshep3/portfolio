<?php
  /**
   * class.PortfolioSqlQuery.php
   *
   * Portfolio website - Chris Shepherd
   *
   * @author Chris Shepherd - chris_shepherd2@hotmail.com
   *
   * @package portfolio
   */

  class PortfolioSqlQuery
  {

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    public function __construct(){}

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    public function __destruct(){}

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    // public static function query_get_error_logging_query_string()
    // {
    //   $m_sql_query_string  = 'INSERT INTO portfolio_error_log';
    //   $m_sql_query_string .= ' SET log_message = :logmessage';
    //   return $m_sql_query_string;
    // }

    // ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    public static function query_get_menu_sections()
    {
        $m_sql_query_string  = 'SELECT menu_sectionID, menu_section_title';
        $m_sql_query_string .= ' FROM menu_section';
        return $m_sql_query_string;
    }

    // ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    public static function query_get_category_for_title()
    {
        $m_sql_query_string  = 'SELECT menu_section_title';
        $m_sql_query_string .= ' FROM menu_section';
        $m_sql_query_string .= ' WHERE menu_sectionID = :menu_sectionID';
        $m_sql_query_string .= ' LIMIT 1';
        return $m_sql_query_string;
    }
    
    // ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    public static function query_get_tag_for_title()
    {
        $m_sql_query_string  = 'SELECT tag';
        $m_sql_query_string .= ' FROM tag';
        $m_sql_query_string .= ' WHERE tagID = :tagID';
        $m_sql_query_string .= ' LIMIT 1';
        return $m_sql_query_string;
    }

    // ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    public static function query_get_project_details()
    {
        $m_sql_query_string  = 'SELECT project_title, screenshot_source,';
        $m_sql_query_string .= ' project_description, project_please_note,';
        $m_sql_query_string .= ' project_download1_text, project_download1_source, project_download2_text, project_download2_source';
        $m_sql_query_string .= ' FROM project';
        $m_sql_query_string .= ' WHERE projectID = :projectID';
        $m_sql_query_string .= ' LIMIT 1';
        return $m_sql_query_string;
    }
    
// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    public static function query_get_all_projects()
    {
        $m_sql_query_string  = 'SELECT projectID, project_title, menu_title, menu_section';
        $m_sql_query_string .= ' FROM project';
        $m_sql_query_string .= ' ORDER BY menu_section, menu_order';
        return $m_sql_query_string;
    }    

    public static function query_get_projects_from_category()
    {
        $m_sql_query_string  = 'SELECT projectID, project_title, screenshot_source, project_please_note';
        $m_sql_query_string .= ' FROM project';
        $m_sql_query_string .= ' WHERE menu_section = :menusection';
        $m_sql_query_string .= ' ORDER BY menu_order';
        return $m_sql_query_string;
    }
    
    public static function query_get_projects_from_tag()
    {
        $m_sql_query_string  = 'SELECT project.projectID, project.project_title, project.screenshot_source, project.project_please_note';
        $m_sql_query_string .= ' FROM project';
        $m_sql_query_string .= ' INNER JOIN project_tag';
        $m_sql_query_string .= ' ON project.projectID = project_tag.projectID';
        $m_sql_query_string .= ' WHERE project_tag.tagID = :tagID';
        $m_sql_query_string .= ' ORDER BY menu_order';
        return $m_sql_query_string;
    }

    public static function query_get_jobs_for_menu()
    {
        $m_sql_query_string  = 'SELECT jobID, company_name';
        $m_sql_query_string .= ' FROM job';
        return $m_sql_query_string;
    }

    // ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    public static function query_get_job_details()
    {
        $m_sql_query_string  = 'SELECT company_name, job_title, dates,';
        $m_sql_query_string .= ' job_description, company_website, company_logo_source';
        $m_sql_query_string .= ' FROM job';
        $m_sql_query_string .= ' WHERE jobID = :jobID';
        $m_sql_query_string .= ' LIMIT 1';
        return $m_sql_query_string;
    }

    public static function query_get_jobs()
    {
        $m_sql_query_string  = 'SELECT jobID, company_name, job_title, dates,';
        $m_sql_query_string .= ' company_logo_source';
        $m_sql_query_string .= ' FROM job';
        return $m_sql_query_string;
    }

    // ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    public static function query_get_placement_details()
    {
        $m_sql_query_string  = 'SELECT screenshot_source,';
        $m_sql_query_string .= ' company_name, job_title, dates, job_description,';
        $m_sql_query_string .= ' company_website, download_source';
        $m_sql_query_string .= ' FROM placement';
        $m_sql_query_string .= ' LIMIT 1';
        return $m_sql_query_string;
    }

    public static function query_get_project_tags()
    {
        $m_sql_query_string  = 'SELECT project_tag.tagID, tag.tag';
        $m_sql_query_string .= ' FROM project_tag';
        $m_sql_query_string .= ' INNER JOIN tag';
        $m_sql_query_string .= ' ON tag.tagID = project_tag.tagID';
        $m_sql_query_string .= ' WHERE projectID = :projectID';
        $m_sql_query_string .= ' ORDER BY tag';
        return $m_sql_query_string;
    }
    
    public static function query_get_job_tags()
    {
        $m_sql_query_string  = 'SELECT job_tag.tagID, tag.tag';
        $m_sql_query_string .= ' FROM job_tag';
        $m_sql_query_string .= ' INNER JOIN tag';
        $m_sql_query_string .= ' ON tag.tagID = job_tag.tagID';
        $m_sql_query_string .= ' WHERE jobID = :jobID';
        $m_sql_query_string .= ' ORDER BY tag';
        return $m_sql_query_string;
    }
  }
?>
