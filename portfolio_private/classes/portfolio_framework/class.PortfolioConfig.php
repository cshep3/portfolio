<?php
  /**
   * class.PortfolioConfig.php
   *
   * Portfolio website - Chris Shepherd
   *
   * @author Chris Shepherd - chris_shepherd2@hotmail.com
   *
   * @package portfolio
   */

  class PortfolioConfig
  {
// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    public function __construct(){}

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    public function __destruct(){}

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    public static function do_definitions()
    {
      define ('DIRSEP', DIRECTORY_SEPARATOR);
      define ('URLSEP', '/');

      $m_class_path = realpath(dirname(__FILE__));
      $m_arr_class_path = explode(DIRSEP, $m_class_path, -1);
      $m_class_file_path = implode(DIRSEP, $m_arr_class_path) . DIRSEP;

      $m_app_root_path = $_SERVER['PHP_SELF'];
      $m_document_root = $_SERVER['HTTP_HOST'];
      $m_arr_app_root_path = explode(URLSEP, $m_app_root_path, -1);
      $m_app_root_path = implode(URLSEP, $m_arr_app_root_path) . URLSEP;
      $m_app_root_path = 'https://' . $m_document_root . $m_app_root_path;

      $m_application_name = 'Portfolio';
      $m_media_path = $m_app_root_path . 'media' . URLSEP;
      $m_projectpics_path = $m_media_path . 'project_screenshots' . URLSEP;
      $m_companylogos_path = $m_media_path . 'company_logos' . URLSEP;
      $m_placement_path = $m_media_path . 'placement' . URLSEP;
      $m_project_path = $m_media_path . 'projects' . URLSEP;
      $m_css_path = 'css' . URLSEP;
      $m_css_file_name = 'portfolio.css';
      $m_js_path = 'js' . URLSEP;
      $m_js_file_name = 'portfolio_functions.js';
      $m_class_file_name = 'class.';

      define ('CLASS_PATH', $m_class_file_path);
      define ('APP_ROOT_PATH', $m_app_root_path);
      define ('APP_NAME', $m_application_name);
      define ('PETPIC_PATH', 'media/petpics/');
      define ('MEDIA_PATH', $m_media_path);
      define ('PROJECTPICS_PATH', $m_projectpics_path);
      define ('COMPANYLOGOS_PATH', $m_companylogos_path);
      define ('CSS_PATH' , $m_css_path);
      define ('CSS_FILE_NAME', $m_css_file_name);
      define ('JS_PATH' , $m_js_path);
      define ('JS_FILE_NAME', $m_js_file_name);
      define ('PLACEMENT_FILES_NAME', $m_placement_path);
      define ('PROJECT_FILES_NAME', $m_project_path);
      define ('CLASS_FILE', $m_class_file_name);
    }

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    public static function get_user_database_connection_details()
    {
      $m_rdbms = 'mysql';
      $m_host = 'localhost';
      $m_port = '3306';
      $m_db_name = 'portfolio_db';
      $m_host_name = $m_rdbms . ':host=' . $m_host. ';port=' . $m_port . ';dbname=' . $m_db_name;
      $m_user_name = 'portfoliouser';
      $m_user_password = 'portfolio_pass123';
      $m_arr_db_connect_details['host_name'] = $m_host_name;
      $m_arr_db_connect_details['user_name'] = $m_user_name;
      $m_arr_db_connect_details['user_password'] = $m_user_password;
      return $m_arr_db_connect_details;
    }
  }
?>
