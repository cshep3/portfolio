<?php
  /**
   * class.PortfolioModelAbstract.php
   *
   * Portfolio website - Chris Shepherd
   *
   * @author Chris Shepherd - chris_shepherd2@hotmail.com
   *
   * @package portfolio
   */

  abstract class PortfolioModelAbstract
  {
    protected $c_obj_database_handle;

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    public function __construct()
    {
        $m_arr_database_connection_details = PortfolioConfig::get_user_database_connection_details();
        $this->c_obj_database_handle = PortfolioContainer::make_portfolio_database_wrapper($m_arr_database_connection_details);
    }

    public final function __destruct(){}

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*    
    public function get_database_connection_result()
    {
        return $this->c_obj_database_handle->get_connection_messages();
    }
  }
?>
