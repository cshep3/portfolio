<?php
  /**
   * class.PortfolioValidateAbstract.php
   *
   * Portfolio website - Chris Shepherd
   *
   * @author Chris Shepherd - chris_shepherd2@hotmail.com
   *
   * @package portfolio
   */

  abstract class PortfolioValidateAbstract
  {
    protected $c_arr_tainted;
    protected $c_arr_cleaned;

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    public final function __construct()
    {
        $this->c_arr_tainted = array();
        $this->c_arr_cleaned = array();
    }

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    public final function __destruct(){}

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    public final function get_sanitised_input()
    {
        return $this->c_arr_cleaned;
    }

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    abstract public function do_sanitise_input();
  }
?>
