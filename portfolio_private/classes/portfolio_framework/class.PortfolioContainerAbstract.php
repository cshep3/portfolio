<?php
  /**
   * class.PortfolioContainerAbstract.php
   *
   * Portfolio website - Chris Shepherd
   *
   * @author Chris Shepherd - chris_shepherd2@hotmail.com
   *
   * @package portfolio
   */

  abstract class PortfolioContainerAbstract
  {
// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    public function __construct($p_feature_name, $p_directory_path)
    {
      $m_controller_file = $p_directory_path . CLASS_FILE . APP_NAME . $p_feature_name . 'Controller.php';
      if(file_exists($m_controller_file))
      {
        include_once $m_controller_file;
      }
      
      $m_view_file = $p_directory_path . CLASS_FILE . APP_NAME . $p_feature_name . 'View.php';
      if(file_exists($m_view_file))
      {
        include_once $m_view_file;
      }
      
      $m_model_file = $p_directory_path . CLASS_FILE . APP_NAME . $p_feature_name . 'Model.php';
      if(file_exists($m_model_file))
      {
        include_once $m_model_file;
      }
      
      $m_validate_file = $p_directory_path . CLASS_FILE . APP_NAME . $p_feature_name . 'Validate.php';
      if(file_exists($m_validate_file))
      {
        include_once $m_validate_file;
      }
    }

    public final function __destruct(){}
  }
?>
