<?php
  /**
   * class.PortfolioContainer.php
   *
   * Portfolio website - Chris Shepherd
   *
   * @author Chris Shepherd - chris_shepherd2@hotmail.com
   *
   * @package portfolio
   */

  class PortfolioContainer
  {
// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    public function __construct(){}

    public function __destruct(){}

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    public static function make_portfolio_router()
    {
      $m_obj_router = new PortfolioRouter();
      $m_obj_router->do_routing();
      $m_html_result = $m_obj_router->get_html_output();
      return $m_html_result;
    }

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    public static function make_portfolio_error($p_error_type)
    {
      $m_obj_error_container = new PortfolioErrorContainer('Error');
      $m_obj_error = new PortfolioErrorController($m_obj_error_container);
      $m_obj_error->set_error_type($p_error_type);
      $m_obj_error->do_process_error();
      $m_obj_error->do_create_output();
      $m_error_message = $m_obj_error->get_html_output();
      return $m_error_message;
    }

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    public static function make_portfolio_process_output($p_html_result)
    {
      $m_obj_process_output = new PortfolioProcessOutput();
      $m_obj_process_output->do_output($p_html_result);
    }

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    public static function make_portfolio_database_wrapper($p_arr_connection_settings)
    {
      $m_obj_database_handle = new PortfolioDatabaseWrapper();
      $m_obj_database_handle->set_connection_settings($p_arr_connection_settings);
      $m_obj_database_handle->do_connect_to_database();
      $m_arr_database_connection_messages = $m_obj_database_handle->get_connection_messages();
      return $m_obj_database_handle;
    }
}
?>
