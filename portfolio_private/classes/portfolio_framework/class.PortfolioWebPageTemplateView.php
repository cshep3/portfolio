<?php
  /**
   * class.PortfolioWebPageTemplateView.php
   *
   * Portfolio website - Chris Shepherd
   *
   * @author Chris Shepherd - chris_shepherd2@hotmail.com
   *
   * @package portfolio
   */

  class PortfolioWebPageTemplateView extends PortfolioViewAbstract
  {
    private $c_menu_bar;
    protected $c_page_title;
    protected $c_html_page_content;
    protected $c_html_page_output;
    private $c_arr_menu_section;
    private $c_arr_menu_section_projects;
    private $c_arr_menu_section_jobs;
    private $c_obj_database_handle;

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    public function __construct()
    {
      $this->c_page_title = '';
      $this->c_html_page_content = '';
      $this->c_html_page_output = '';
      $this->c_menu_bar = '';
      $this->c_arr_menu_section = array();
      $this->c_arr_menu_section_projects = array();
      $this->c_arr_menu_section_jobs = array();
      $this->c_obj_database_handle = null;
    }

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    // public function __destruct(){}

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    public function create_web_page()
    {
      $this->create_web_page_meta_headings();
      $this->create_menu_bar();
      $this->insert_page_content();
      $this->create_web_page_footer();
    }

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    private function create_web_page_meta_headings()
    {
      $m_css_filename = CSS_PATH . CSS_FILE_NAME;
      $m_css_mobile_filename = CSS_PATH . "mobile.css";
      $m_css_slicknav_filename = CSS_PATH . "slicknav.css";
      $m_js_filename = JS_PATH . JS_FILE_NAME;
      $m_icon_path = MEDIA_PATH . "favicon.ico";
      $m_html_output = <<< HTML
<!DOCTYPE html>
<html lang="en">
<head>
<title>$this->c_page_title</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="Content-Language" content="en-gb" />
<meta name="author" content="Chris Shepherd" />  
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://bootswatch.com/united/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<link rel="stylesheet" href="$m_css_filename" type="text/css" />
<link rel="icon" href="$m_icon_path">
<script type="text/JavaScript" src="$m_js_filename"></script>
</head>
<body>
HTML;
      $this->c_html_page_output .= $m_html_output;
    }

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    private function insert_page_content()
    {
      $m_landing_page = APP_ROOT_PATH;
      $m_html_output = <<< HTML
<!-- Header -->      
$this->c_menu_bar
<div class="jumbotron">
  <div class="container text-center">
    <h1>Chris Shepherd</h1>
    <p>Strongly motivated software developer with 2 years of professional experience</p>
	<p><a href="mailto:chris_shepherd2@hotmail.com"><span class="glyphicon glyphicon-envelope"></span> chris_shepherd2@hotmail.com</a></p>
	<p><a href="tel:+447929672636"><span class="glyphicon glyphicon-phone"></span> +447929672636</a></p>
  </div>
</div>

<form id="navbarform" method="get" action="$m_landing_page">
<input type="hidden" name="feature" id="feature" value="-1"> 
<input type="hidden" name="index" id="index" value="-1">
</form>

<!-- Header -->

<!-- Content -->

$this->c_html_page_content

<!-- Content -->
HTML;
      $this->c_html_page_output .= $m_html_output;
    }

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    private function create_menu_bar()
    {
      $m_menu_option_buttons = '';
      $m_arr_menu_options = array();
      $this->do_create_menu_section_list();
      $m_landing_page = APP_ROOT_PATH;

      $m_menu_section_count = count($this->c_arr_menu_section);
      $this->do_create_menu_section_jobs_list();
      $this->c_arr_menu_section_projects = $this->do_create_menu_section_projects_list();
      
      $this->c_menu_bar = <<< MENUBAR
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href=$m_landing_page>Portfolio</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li><a href=$m_landing_page>Home</a></li>      
MENUBAR;

        for ($i=1;$i<$m_menu_section_count+1;$i++)
        {
            if ($i != 6) //placement isn't collapsable
            {
                $m_section_title = $this->c_arr_menu_section[$i];
                $this->c_menu_bar .= <<< MENUBAR
<li class="dropdown">
<a class="dropdown-toggle" data-toggle="dropdown" href="javascript:submitNavBarForm(document.getElementById('navbarform'),'display_projects',$i);">$m_section_title<span class="caret"></span></a>
<ul class="dropdown-menu">
MENUBAR;
                if ($i != 7) // projects
                {
                    foreach ($this->c_arr_menu_section_projects as $m_project)
                    {
                        if ($m_project['menu_section'] == $i)
                        {
                            $m_title = $m_project['menu_title'];
                            $m_project_id = $m_project['projectID'];
                            $this->c_menu_bar .= <<< MENUBAR
<li><a href="javascript:submitNavBarForm(document.getElementById('navbarform'),'display_project_details',$m_project_id);">$m_title</a></li>
MENUBAR;
                        }
                    }
                $this->c_menu_bar .= <<< MENUBAR
<li><a href="javascript:submitNavBarForm(document.getElementById('navbarform'),'display_projects',$i);">View All</a></li>
</ul>
</li>               
MENUBAR;
                }
                else // jobs
                {
                    foreach ($this->c_arr_menu_section_jobs as $m_job)
                    {
                        $m_title = $m_job['company_name'];
                        $m_job_id = $m_job['jobID'];
                        $this->c_menu_bar .= <<< MENUBAR
<li><a href="javascript:submitNavBarForm(document.getElementById('navbarform'),'display_job_details',$m_job_id);">$m_title</a></li>
MENUBAR;
                    }                    
                $this->c_menu_bar .= <<< MENUBAR
<li><a href="javascript:submitNavBarForm(document.getElementById('navbarform'),'display_jobs',-1);">View All</a></li>
</ul>
</li>               
MENUBAR;
                }
            }
            else
            {
                $m_section_title = $this->c_arr_menu_section[$i];
                $this->c_menu_bar .= <<< MENUBAR
<li><a href="javascript:submitNavBarForm(document.getElementById('navbarform'),'display_placement_details',-1);">$m_section_title</a></li>                
MENUBAR;
            }
        }
        
        $this->c_menu_bar .= <<< MENUBAR
        </ul>
    </div>
  </div>
</nav>
MENUBAR;

    }

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    private function create_web_page_footer()
    {
      $m_html_output = <<< HTML
<!-- Footer -->

<footer class="container-fluid text-center">
  <p>
  Developed & Maintained by Chris Shepherd <br>
  <a href="mailto:chris_shepherd2@hotmail.com"><span class="glyphicon glyphicon-envelope"></span> chris_shepherd2@hotmail.com</a><br>
  <a href="tel:+447929672636"><span class="glyphicon glyphicon-phone"></span> +447929672636</a>
  </p>
</footer>

<!-- Footer -->
HTML;
      $this->c_html_page_output .= $m_html_output;
    }

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    public function do_create_menu_section_list()
    {

        $m_arr_database_connection_details = PortfolioConfig::get_user_database_connection_details();
        $this->c_obj_database_handle = PortfolioContainer::make_portfolio_database_wrapper($m_arr_database_connection_details);

        $m_arr_menu_section = array();
        $m_sql_query_string = PortfolioSqlQuery::query_get_menu_sections();
        $m_arr_sql_query_parameters = array();

        $this->c_obj_database_handle->safe_query($m_sql_query_string, $m_arr_sql_query_parameters);

        $m_number_of_sections = $this->c_obj_database_handle->count_rows();

        if ($m_number_of_sections > 0)
        {
            while ($m_row = $this->c_obj_database_handle->safe_fetch_array())
            {
                $m_menu_section_id = $m_row['menu_sectionID'];
                $m_menu_section_name = $m_row['menu_section_title'];
                $m_arr_menu_section[$m_menu_section_id] = $m_menu_section_name;
            }
        }
        $this->c_arr_menu_section = $m_arr_menu_section;
    }

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    public function do_create_menu_section_projects_list()
    {
        $m_arr_menu_section_projects = array();
        $m_sql_query_string = PortfolioSqlQuery::query_get_all_projects();
        $this->c_obj_database_handle->safe_query($m_sql_query_string);

        $m_number_of_sections = $this->c_obj_database_handle->count_rows();

        if ($m_number_of_sections > 0)
        {
            $m_arr_menu_section_projects = $this->c_obj_database_handle->safe_fetch_all_results();
        }
        return $m_arr_menu_section_projects;
    }

    public function do_create_menu_section_jobs_list()
    {
        $m_arr_menu_section_jobs = array();
        $m_sql_query_string = PortfolioSqlQuery::query_get_jobs_for_menu();
        $this->c_obj_database_handle->safe_query($m_sql_query_string);

        $m_number_of_jobs = $this->c_obj_database_handle->count_rows();
        if ($m_number_of_jobs > 0)
        {
            $m_arr_menu_section_jobs = $this->c_obj_database_handle->safe_fetch_all_results();
        }
        $this->c_arr_menu_section_jobs = $m_arr_menu_section_jobs;
    }
  }
?>
