<?php
/**
 * class.PortfolioDisplayProjectsValidate.php
 *
 * Portfolio website - Chris Shepherd
 *
 * @author Chris Shepherd - chris_shepherd2@hotmail.com
 *
 * @package portfolio
 */

class PortfolioDisplayProjectsValidate extends PortfolioValidateAbstract
{
    public function do_sanitise_input()
    {
        $m_no_index_selected_error = true;
        $m_validated_index = false;
        $this->c_arr_tainted = $_GET;
        $m_valid_details = false;
        $m_error_count = 0;

        if (isset($this->c_arr_tainted['index']))
        {
            $m_index_to_validate = $this->c_arr_tainted['index'];

            if ($m_index_to_validate == '0')
            {
                $m_error_count += 1;
            }

            if (!is_string($m_index_to_validate))
            {
                $m_error_count += 1;
            }
        }
        else
        {
            $m_error_count += 1;
        }

        if ($m_error_count == 0)
        {
            $m_no_index_selected_error = false;
            $m_validated_index = filter_var($m_index_to_validate, FILTER_SANITIZE_NUMBER_INT);
            $this->c_arr_cleaned['index'] = $m_validated_index;
        }
        $this->c_arr_cleaned['no-index-selected-error'] = $m_no_index_selected_error;
    }
}
?>
