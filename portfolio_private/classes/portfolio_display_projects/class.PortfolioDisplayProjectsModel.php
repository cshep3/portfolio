<?php
/**
 * class.PortfolioDisplayProjectsModel.php
 *
 * Portfolio website - Chris Shepherd
 *
 * @author Chris Shepherd - chris_shepherd2@hotmail.com
 *
 * @package portfolio
 */

class PortfolioDisplayProjectsModel extends PortfolioModelAbstract
{
    private $c_arr_projects;
    private $c_sanitised_project_index;

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    public function __construct()
    {
        parent::__construct();
        $this->c_arr_projects = array();
    }

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    public function get_project_details()
    {
        return $this->c_arr_projects;
    }

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    public function set_project_index($p_arr_sanitised_input)
    {
        $m_sanitised_project_index = '';
        if (isset($p_arr_sanitised_input['index']))
        {
            $m_sanitised_project_index = $p_arr_sanitised_input['index'];
        }
        $this->c_sanitised_project_index = $m_sanitised_project_index;
    }

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    public function do_retrieve_projects()
    {
        $m_sanitised_project_index = $this->c_sanitised_project_index;
        $m_sql_query_string = PortfolioSqlQuery::query_get_projects_from_category();
        $m_arr_sql_query_parameters = array(':menusection' => $m_sanitised_project_index);
        $m_query_result = $this->c_obj_database_handle->safe_query($m_sql_query_string, $m_arr_sql_query_parameters);

        $m_project_count = $this->c_obj_database_handle->count_rows();

//        var_dump($this->c_obj_database_handle->get_connection_messages());

        if ($m_project_count == 0)
        {
            $m_sanitised_project_index = false;
        }
        else
        {
            $m_projects = $this->c_obj_database_handle->safe_fetch_all_results();
            $this->c_arr_projects['sanitised-project-index'] = $m_sanitised_project_index;
            $this->c_arr_projects['projects'] = $m_projects;
        }
    }

    public function do_retrieve_category()
    {
        $m_sanitised_project_index = $this->c_sanitised_project_index;
        $m_sql_query_string = PortfolioSqlQuery::query_get_category_for_title();
        $m_arr_sql_query_parameters = array(':menu_sectionID' => $m_sanitised_project_index);
        $m_query_result = $this->c_obj_database_handle->safe_query($m_sql_query_string, $m_arr_sql_query_parameters);

        $m_project_count = $this->c_obj_database_handle->count_rows();

        if ($m_project_count != 0)
        {
            $this->c_arr_projects['category'] = $this->c_obj_database_handle->safe_fetch_row();
        }

    }
}
?>
