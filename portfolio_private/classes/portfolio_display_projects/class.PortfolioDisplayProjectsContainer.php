<?php
/**
 * class.PortfolioDisplayProjectsContainer.php
 *
 * Portfolio website - Chris Shepherd
 *
 * @author Chris Shepherd - chris_shepherd2@hotmail.com
 *
 * @package portfolio
 */

  class PortfolioDisplayProjectsContainer extends PortfolioContainerAbstract
  {
// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    public function __construct($p_feature_name)
    {
      $m_directory_path = realpath(dirname(__FILE__)) . DIRSEP;
      parent::__construct($p_feature_name, $m_directory_path);
    }

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    public function make_portfolio_display_projects_controller()
    {
        $m_obj_self = $this;
        $m_obj_display_projects_controller = new PortfolioDisplayProjectsController($m_obj_self);
        $m_obj_display_projects_controller->do_create_html_output();
        $m_html_output = $m_obj_display_projects_controller->get_html_output();
        return $m_html_output;
    }

    public function make_portfolio_display_projects_validate()
    {
        $m_obj_display_projects_validate = new PortfolioDisplayProjectsValidate();
        $m_obj_display_projects_validate->do_sanitise_input();
        $m_arr_sanitised_input = $m_obj_display_projects_validate->get_sanitised_input();
        return $m_arr_sanitised_input;
    }

    public function make_portfolio_display_projects_model($p_arr_sanitised_input)
    {
        $m_obj_display_projects_model = new PortfolioDisplayProjectsModel();
        $m_obj_display_projects_model->set_project_index($p_arr_sanitised_input);
        $m_obj_display_projects_model->do_retrieve_projects();
        $m_obj_display_projects_model->do_retrieve_category();
        $m_arr_projects = $m_obj_display_projects_model->get_project_details();
        return $m_arr_projects;
    }

    public function make_portfolio_display_projects_view($p_arr_project_details)
    {
        $m_obj_display_projects_view = new PortfolioDisplayProjectsView();
        $m_obj_display_projects_view->set_projects($p_arr_project_details);
        $m_obj_display_projects_view->do_create_output_page();
        $m_html_output = $m_obj_display_projects_view->get_html_output();
        return $m_html_output;
    }
  }
?>
