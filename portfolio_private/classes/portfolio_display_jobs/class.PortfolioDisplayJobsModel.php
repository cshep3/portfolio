<?php
/**
 * class.PortfolioDisplayJobsModel.php
 *
 * Portfolio website - Chris Shepherd
 *
 * @author Chris Shepherd - chris_shepherd2@hotmail.com
 *
 * @package portfolio
 */

class PortfolioDisplayJobsModel extends PortfolioModelAbstract
{
    private $c_arr_jobs;

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    public function __construct()
    {
        parent::__construct();
        $this->c_arr_jobs = array();
    }

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    public function get_job_details()
    {
        return $this->c_arr_jobs;
    }

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    public function do_retrieve_jobs()
    {
        $m_sql_query_string = PortfolioSqlQuery::query_get_jobs();
        $m_query_result = $this->c_obj_database_handle->safe_query($m_sql_query_string);

        $m_job_count = $this->c_obj_database_handle->count_rows();

        if ($m_job_count == 0)
        {
        }
        else
        {
            $m_jobs = $this->c_obj_database_handle->safe_fetch_all_results();
            $this->c_arr_jobs = $m_jobs;
        }
    }
}
?>
