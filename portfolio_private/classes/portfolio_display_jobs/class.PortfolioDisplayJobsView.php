<?php
/**
 * class.PortfolioDisplayJobsView.php
 *
 * Portfolio website - Chris Shepherd
 *
 * @author Chris Shepherd - chris_shepherd2@hotmail.com
 *
 * @package portfolio
 */

class PortfolioDisplayJobsView extends PortfolioWebPageTemplateView
{
    private $c_arr_jobs;

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    public function __construct()
    {
        $this->c_arr_jobs = array();
    }

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    public function set_jobs($p_arr_jobs)
    {
        $this->c_arr_jobs = $p_arr_jobs;
    }

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    protected function do_assign_page_title()
    {
        $this->c_page_title = 'Employment History';
    }
// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    protected function do_create_relevant_output()
    {
        $this->do_display_jobs();
    }

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    private function do_create_error_message()
    {
        $m_address = APP_ROOT_PATH;
        $this->c_html_page_content = <<< NAMEERRORPAGE
<div class="container text-center">
<div class="well">
<div class="row">
<h1>Error</h1>
<div class="col-sm-12">
<div class="well">
<h3 class="error">Oops - there was a problem with the category
you selected/entered - Please try again.</h3>
<p>If you think this could be a problem with the site please
contact: <a href="mailto:chris_shepherd2@hotmail.com">chris_shepherd2@hotmail.com</a>.</p>
<h4><a href=$m_address>Return to Home Page</a></h4>
</div>
</div>
</div>
</div>
</div>
NAMEERRORPAGE;
    }

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    private function do_display_jobs()
    {
        $m_address = APP_ROOT_PATH;

        $m_arr_jobs = $this->c_arr_jobs;

        $m_title = "Employment History";

        $this->c_html_page_content = <<< PROJECTS
<div class="container text-center">
<div class="well">
  <div class="row">
	<h1>$m_title</h1>
PROJECTS;
        $col = 0;
        foreach ($m_arr_jobs as $m_job)
        {
            $col++;
            if ($col > 3)
            {
                $this->c_html_page_content .= <<< JOBS
<div class="row">
JOBS;
            }
            $m_job_id = $m_job['jobID'];
            $m_company_name = $m_job['company_name'];
            $m_job_logo_source = COMPANYLOGOS_PATH . $m_job['company_logo_source'];
            $m_job_title = $m_job['job_title'];
            $m_job_dates = $m_job['dates'];

            $m_job_link = "javascript:submitNavBarForm(document.getElementById('navbarform'),'display_job_details',$m_job_id);";

            $m_job_screenshot_name = $m_company_name . ' logo';

            $this->c_html_page_content .= <<< JOBS
<div class="col-sm-4">
    <a href=$m_job_link class="thumbnail" style="height:180px">
      <h4>$m_company_name <br>
      <small>$m_job_title</small></h4> 
      <img src=$m_job_logo_source alt=$m_job_screenshot_name style="max-width:200px;height:50px">
      <h4><small>$m_job_dates</small></h4>
    </a>
</div>
JOBS;
            if ($col == 3)
            {
                $this->c_html_page_content .= <<< JOBS
</div>
JOBS;
            }
        }
        
        $this->c_html_page_content .= <<< JOBS
  </div>
</div>
</div>
JOBS;

    }

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    private function convert_date_format($p_date_to_convert)
    {
        return date('d/n/Y', strtotime($p_date_to_convert));
    }
}
?>
