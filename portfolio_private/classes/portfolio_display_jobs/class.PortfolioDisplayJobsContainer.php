<?php
/**
 * class.PortfolioDisplayJobsContainer.php
 *
 * Portfolio website - Chris Shepherd
 *
 * @author Chris Shepherd - chris_shepherd2@hotmail.com
 *
 * @package portfolio
 */

  class PortfolioDisplayJobsContainer extends PortfolioContainerAbstract
  {
// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    public function __construct($p_feature_name)
    {
      $m_directory_path = realpath(dirname(__FILE__)) . DIRSEP;
      parent::__construct($p_feature_name, $m_directory_path);
    }

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    public function make_portfolio_display_jobs_controller()
    {
        $m_obj_self = $this;
        $m_obj_display_job_details_controller = new PortfolioDisplayJobsController($m_obj_self);
        $m_obj_display_job_details_controller->do_create_html_output();
        $m_html_output = $m_obj_display_job_details_controller->get_html_output();
        return $m_html_output;
    }

    public function make_portfolio_display_jobs_model()
    {
        $m_obj_display_job_details_model = new PortfolioDisplayJobsModel();
        $m_obj_display_job_details_model->do_retrieve_jobs();
        $m_arr_job_details = $m_obj_display_job_details_model->get_job_details();
        return $m_arr_job_details;
    }

    public function make_portfolio_display_jobs_view($p_arr_jobs)
    {
        $m_obj_display_job_details_view = new PortfolioDisplayJobsView();
        $m_obj_display_job_details_view->set_jobs($p_arr_jobs);
        $m_obj_display_job_details_view->do_create_output_page();
        $m_html_output = $m_obj_display_job_details_view->get_html_output();
        return $m_html_output;
    }
  }
?>
