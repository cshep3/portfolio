<?php
/**
 * class.PortfolioDisplayProjectTagsContainer.php
 *
 * Portfolio website - Chris Shepherd
 *
 * @author Chris Shepherd - chris_shepherd2@hotmail.com
 *
 * @package portfolio
 */

  class PortfolioDisplayProjectTagsContainer extends PortfolioContainerAbstract
  {
// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    public function __construct($p_feature_name)
    {
      $m_directory_path = realpath(dirname(__FILE__)) . DIRSEP;
      parent::__construct($p_feature_name, $m_directory_path);
    }

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    public function make_portfolio_display_project_tags_controller()
    {
        $m_obj_self = $this;
        $m_obj_display_project_tags_controller = new PortfolioDisplayProjectTagsController($m_obj_self);
        $m_obj_display_project_tags_controller->do_create_html_output();
        $m_html_output = $m_obj_display_project_tags_controller->get_html_output();
        return $m_html_output;
    }

    public function make_portfolio_display_project_tags_validate()
    {
        $m_obj_display_project_tags_validate = new PortfolioDisplayProjectTagsValidate();
        $m_obj_display_project_tags_validate->do_sanitise_input();
        $m_arr_sanitised_input = $m_obj_display_project_tags_validate->get_sanitised_input();
        return $m_arr_sanitised_input;
    }

    public function make_portfolio_display_project_tags_model($p_arr_sanitised_input)
    {
        $m_obj_display_project_tags_model = new PortfolioDisplayProjectTagsModel();
        $m_obj_display_project_tags_model->set_project_index($p_arr_sanitised_input);
        $m_obj_display_project_tags_model->do_retrieve_projects();
        $m_obj_display_project_tags_model->do_retrieve_category();
        $m_arr_projects = $m_obj_display_project_tags_model->get_project_details();
        return $m_arr_projects;
    }

    public function make_portfolio_display_project_tags_view($p_arr_project_details)
    {
        $m_obj_display_project_tags_view = new PortfolioDisplayProjectTagsView();
        $m_obj_display_project_tags_view->set_projects($p_arr_project_details);
        $m_obj_display_project_tags_view->do_create_output_page();
        $m_html_output = $m_obj_display_project_tags_view->get_html_output();
        return $m_html_output;
    }
  }
?>
