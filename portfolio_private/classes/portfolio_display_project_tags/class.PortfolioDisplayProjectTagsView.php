<?php
/**
 * class.PortfolioDisplayProjectTagsView.php
 *
 * Portfolio website - Chris Shepherd
 *
 * @author Chris Shepherd - chris_shepherd2@hotmail.com
 *
 * @package portfolio
 */

class PortfolioDisplayProjectTagsView extends PortfolioWebPageTemplateView
{
    private $c_arr_projects;

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    public function __construct()
    {
        $this->c_arr_projects = array();
    }

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    public function set_projects($p_arr_projects)
    {
        $this->c_arr_projects = $p_arr_projects;
    }

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    protected function do_assign_page_title()
    {
        if (isset($this->c_arr_projects['sanitised-project-index'])) {
            if (!$this->c_arr_projects['sanitised-project-index']) {
                $this->c_page_title = "Error";
            } else {
                $m_category = $this->c_arr_projects['category'][0];
                $this->c_page_title = "$m_category ProjectTags";
            }
        } else {
            $this->c_page_title = "Error";
        }
    }
// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    protected function do_create_relevant_output()
    {
        if (isset($this->c_arr_projects['sanitised-project-index']))
        {
            if (!$this->c_arr_projects['sanitised-project-index'])
            {
                $this->do_create_error_message();
            }
            else
            {
                $this->do_display_projects();
            }
        }
        else
        {
            $this->do_create_error_message();
        }
    }

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    private function do_create_error_message()
    {
        $m_address = APP_ROOT_PATH;
        $this->c_html_page_content = <<< NAMEERRORPAGE
<div class="container text-center">
<div class="well">
<div class="row">
<h1>Error</h1>
<div class="col-sm-12">
<div class="well">
<h3 class="error">Oops - there was a problem with the tag
you selected/entered - Please try again.</h3>
<p>If you think this could be a problem with the site please
contact: <a href="mailto:chris_shepherd2@hotmail.com">chris_shepherd2@hotmail.com</a>.</p>
<h4><a href=$m_address>Return to Home Page</a></h4>
</div>
</div>
</div>
</div>
</div>
NAMEERRORPAGE;
    }

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    private function do_display_projects()
    {
        $m_stock_values = '';
        $m_address = APP_ROOT_PATH;

        $m_project_index = $this->c_arr_projects['sanitised-project-index'];
        $m_arr_projects = $this->c_arr_projects['projects'];

        $m_tag = $this->c_arr_projects['tag'][0];
        $m_title = "Projects Tagged: " . $m_tag;

        $this->c_html_page_content = <<< PROJECTS
<div class="container text-center">
<div class="well">
  <div class="row">
	<h1>$m_title</h1>
PROJECTS;
        $col = 0;
        foreach ($m_arr_projects as $m_project)
        {
            $col++;
            if ($col > 3)
            {
                $this->c_html_page_content .= <<< PROJECTS
<div class="row">
PROJECTS;
                $col = 1;
            }
            $m_project_id = $m_project['projectID'];
            $m_project_title = $m_project['project_title'];
            $m_project_screenshot_source = PROJECTPICS_PATH . $m_project['screenshot_source'];
            $m_project_please_note = $m_project['project_please_note'];
            $m_project_link = "javascript:submitNavBarForm(document.getElementById('navbarform'),'display_project_details',$m_project_id);";
            $m_project_screenshot_name = $m_project_title . ' screenshot';

            $this->c_html_page_content .= <<< PROJECTS
<div class="col-sm-4">
    <a href=$m_project_link class="thumbnail">
      <h4>$m_project_title <br>
      <small>$m_project_please_note</small></h4> 
      <img src=$m_project_screenshot_source alt=$m_project_screenshot_name style="height:120px">
    </a>
</div>
PROJECTS;

            if ($col == 3)
            {
                $this->c_html_page_content .= <<< PROJECTS
</div>
PROJECTS;
            }
        }
        
        $this->c_html_page_content .= <<< PROJECTS
  </div>
</div>
</div>
PROJECTS;
    }

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    private function convert_date_format($p_date_to_convert)
    {
        return date('d/n/Y', strtotime($p_date_to_convert));
    }
}
?>
