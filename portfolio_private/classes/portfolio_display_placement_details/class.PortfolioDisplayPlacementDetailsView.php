<?php
/**
 * class.PortfolioDisplayPlacementDetailsView.php
 *
 * Portfolio website - Chris Shepherd
 *
 * @author Chris Shepherd - chris_shepherd2@hotmail.com
 *
 * @package portfolio
 */

class PortfolioDisplayPlacementDetailsView extends PortfolioWebPageTemplateView
{
    private $c_arr_placment_details;

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    public function __construct()
    {
        $this->c_arr_placment_details = array();
    }

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    public function set_placement_details($p_arr_placement_details)
    {
        $this->c_arr_placment_details = $p_arr_placement_details;
    }

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    protected function do_assign_page_title()
    {
        $this->c_page_title = 'Placement Year';
    }
// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    protected function do_create_relevant_output()
    {
        $this->do_display_placement_details();
    }

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    private function do_create_error_message()
    {
        $m_address = APP_ROOT_PATH;
        $this->c_html_page_content = <<< NAMEERRORPAGE
<div class="container text-center">
<div class="well">
<div class="row">
<h1>Error</h1>
<div class="col-sm-12">
<div class="well">
<h3 class="error">Oops - there was a problem with the category
you selected/entered - Please try again.</h3>
<p>If you think this could be a problem with the site please
contact: <a href="mailto:chris_shepherd2@hotmail.com">chris_shepherd2@hotmail.com</a>.</p>
<h4><a href=$m_address>Return to Home Page</a></h4>
</div>
</div>
</div>
</div>
NAMEERRORPAGE;
    }

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    private function do_display_placement_details()
    {
        $m_address = APP_ROOT_PATH;

        $m_arr_placement_details = $this->c_arr_placment_details;

        $m_company_name = "Industrial Placement - " . $m_arr_placement_details['company_name'];
        $m_image_name = $m_company_name . " Logo";
        $m_logo_source = COMPANYLOGOS_PATH . $m_arr_placement_details['screenshot_source'];
        $m_job_title = $m_arr_placement_details['job_title'];
        $m_dates = $m_arr_placement_details['dates'];
        $m_description = $m_arr_placement_details['job_description'];
        $m_company_website = $m_arr_placement_details['company_website'];
        $m_download_source = PLACEMENT_FILES_NAME . $m_arr_placement_details['download_source'];
        
        $this->c_html_page_content = <<< VIEWJOBDETAILS
<div class="container text-center">
<div class="well">
  <div class="row">
	<h1>$m_company_name</h1>
    <div class="col-sm-6 col-sm-push-6">
      <div class="well">
        <h4>Company Logo</h4>
        <img src=$m_logo_source class="img-responsive" alt=$m_image_name width=200>
      </div>
      <div class="well">
        <h4>Skills</h4>
        <p>
          <span class="label label-default">Object Pascal</span>
          <span class="label label-primary">PHP</span>
          <span class="label label-success">OO</span>
          <span class="label label-info">Testing</span>
          <span class="label label-warning">Perforce</span>
        </p>
      </div>
      <div class="well">
        <h4>Dates<br>
        <small>$m_dates</small>
	    </h4>
      </div>
      <div class="well">
        <h4>Company Website<br>
        <small><a href=$m_company_website>$m_company_website</a></small>
		</h4>
      </div> 
      <div class="well">
        <h4>Download<br>
        <small><a href=$m_download_source download>Placement Final Report</a></small>
		</h4>
      </div>
    </div>
	
	<div class="col-sm-6 col-sm-pull-6">
		<h3>$m_job_title</h3>
		$m_description
	</div>
  </div>
</div>
</div>
VIEWJOBDETAILS;

    }
}
?>
