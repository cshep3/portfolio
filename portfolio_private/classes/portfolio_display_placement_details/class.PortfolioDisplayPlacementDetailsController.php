<?php
/**
 * class.PortfolioDisplayPlacementDetailsController.php
 *
 * Portfolio website - Chris Shepherd
 *
 * @author Chris Shepherd - chris_shepherd2@hotmail.com
 *
 * @package portfolio
 */

class PortfolioDisplayPlacementDetailsController extends PortfolioControllerAbstract
{

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    public function do_create_html_output()
    {
        $m_arr_placement_details = $this->c_obj_container->make_portfolio_display_placement_details_model();
        $this->c_html_output = $this->c_obj_container->make_portfolio_display_placement_details_view($m_arr_placement_details);
    }
}
?>
