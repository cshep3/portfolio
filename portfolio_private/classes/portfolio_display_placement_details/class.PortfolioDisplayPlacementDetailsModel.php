<?php
/**
 * class.PortfolioDisplayPlacementDetailsModel.php
 *
 * Portfolio website - Chris Shepherd
 *
 * @author Chris Shepherd - chris_shepherd2@hotmail.com
 *
 * @package portfolio
 */

class PortfolioDisplayPlacementDetailsModel extends PortfolioModelAbstract
{
    private $c_arr_placement_details;

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    public function __construct()
    {
        parent::__construct();
        $this->c_arr_placement_details = array();
    }

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    public function get_placement_details()
    {
        return $this->c_arr_placement_details;
    }
    
// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    public function do_retrieve_placement()
    {
        $m_sql_query_string = PortfolioSqlQuery::query_get_placement_details();
        $m_query_result = $this->c_obj_database_handle->safe_query($m_sql_query_string);

        $m_project_count = $this->c_obj_database_handle->count_rows();

//        var_dump($this->c_obj_database_handle->get_connection_messages());

        if ($m_project_count == 0)
        {
        }
        else
        {
            $m_placement = $this->c_obj_database_handle->safe_fetch_array();
            $this->c_arr_placement_details = $m_placement;
        }
    }
}
?>
