<?php
/**
 * class.PortfolioDisplayPlacementDetailsContainer.php
 *
 * Portfolio website - Chris Shepherd
 *
 * @author Chris Shepherd - chris_shepherd2@hotmail.com
 *
 * @package portfolio
 */

  class PortfolioDisplayPlacementDetailsContainer extends PortfolioContainerAbstract
  {
// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    public function __construct($p_feature_name)
    {
      $m_directory_path = realpath(dirname(__FILE__)) . DIRSEP;
      parent::__construct($p_feature_name, $m_directory_path);
    }

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    public function make_portfolio_display_placement_details_controller()
    {
        $m_obj_self = $this;
        $m_obj_display_placement_details_controller = new PortfolioDisplayPlacementDetailsController($m_obj_self);
        $m_obj_display_placement_details_controller->do_create_html_output();
        $m_html_output = $m_obj_display_placement_details_controller->get_html_output();
        return $m_html_output;
    }

    public function make_portfolio_display_placement_details_model()
    {
        $m_obj_display_placement_details_model = new PortfolioDisplayPlacementDetailsModel();
        $m_obj_display_placement_details_model->do_retrieve_placement();
        $m_arr_placement_details = $m_obj_display_placement_details_model->get_placement_details();
        return $m_arr_placement_details;
    }

    public function make_portfolio_display_placement_details_view($p_arr_placement_details)
    {
        $m_obj_display_placement_details_view = new PortfolioDisplayPlacementDetailsView();
        $m_obj_display_placement_details_view->set_placement_details($p_arr_placement_details);
        $m_obj_display_placement_details_view->do_create_output_page();
        $m_html_output = $m_obj_display_placement_details_view->get_html_output();
        return $m_html_output;
    }
  }
?>
