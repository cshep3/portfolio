<?php
/**
 * class.PortfolioDisplayProjectDetailsView.php
 *
 * Portfolio website - Chris Shepherd
 *
 * @author Chris Shepherd - chris_shepherd2@hotmail.com
 *
 * @package portfolio
 */

class PortfolioDisplayProjectDetailsView extends PortfolioWebPageTemplateView
{
    private $c_arr_project_details;

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    public function __construct()
    {
        $this->c_arr_project_details = array();
    }

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    public function set_project_details($p_arr_project_details)
    {
        $this->c_arr_project_details = $p_arr_project_details;
    }

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    protected function do_assign_page_title()
    {
        if (isset($this->c_arr_project_details['sanitised-project-index'])) {
            if (!$this->c_arr_project_details['sanitised-project-index']) {
                $this->c_page_title = "Error";
            } else {
                $m_title = $this->c_arr_project_details['project-details']['project_title'];
                $this->c_page_title = $m_title;
            }
        } else {
            $this->c_page_title = "Error";
        }
    }
// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    protected function do_create_relevant_output()
    {
        if (isset($this->c_arr_project_details['sanitised-project-index']))
        {
            if (!$this->c_arr_project_details['sanitised-project-index'])
            {
                $this->do_create_error_message();
            }
            else
            {
                $this->do_display_project_details();
            }
        }
        else
        {
            $this->do_create_error_message();
        }
    }

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    private function do_create_error_message()
    {
        $m_address = APP_ROOT_PATH;
        $this->c_html_page_content = <<< NAMEERRORPAGE
<div class="container text-center">
<div class="well">
<div class="row">
<h1>Error</h1>
<div class="col-sm-12">
<div class="well">
<h3 class="error">Oops - there was a problem with the project
you selected/entered - Please try again.</h3>
<p>If you think this could be a problem with the site please
contact: <a href="mailto:chris_shepherd2@hotmail.com">chris_shepherd2@hotmail.com</a>.</p>
<h4><a href=$m_address>Return to Home Page</a></h4>
</div>
</div>
</div>
</div>
</div>
NAMEERRORPAGE;
    }

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    private function do_display_project_details()
    {
        $m_tags = $this->c_arr_project_details['project-tags'];

        $m_project_index = $this->c_arr_project_details['sanitised-project-index'];
        $m_arr_project_details = $this->c_arr_project_details['project-details'];

        $m_title = $m_arr_project_details['project_title'];
        $m_screenshot_source = PROJECTPICS_PATH . $m_arr_project_details['screenshot_source'];
        $m_description = $m_arr_project_details['project_description'];
        $m_please_note = $m_arr_project_details['project_please_note'];
        $m_download1_text = $m_arr_project_details['project_download1_text'];
        $m_download1_source = PROJECT_FILES_PATH . $m_arr_project_details['project_download1_source'];
        $m_download1 = '';
        $pos = strpos($m_download1_source,'http');
        if ($pos === false)
        {
            $m_download2 = 'download';
        }
        if ($m_arr_project_details['project_download2_text'] != '') {
            $m_download2 = '';
            $m_download2_text = $m_arr_project_details['project_download2_text'];
            $m_download2_source = PROJECT_FILES_PATH . $m_arr_project_details['project_download2_source'];
            $pos = strpos($m_download2_source,'http');
            if ($pos === false)
            {
                $m_download2 = 'download';
            }
        }
        
        $this->c_html_page_content = <<< PROJECT
<div class="container text-center">
<div class="well">
  <div class="row">
	<h1>$m_title</h1>
    <div class="col-sm-6 col-sm-push-6">
      <div class="well">
        <h4>Screenshot</h4>
        <img src=$m_screenshot_source class="img-responsive" alt="Screenshot" style="max-height:350px">
      </div>
      <div class="well">
        <h4>Tags</h4>
        <p>
PROJECT;
            if ($m_tags != null)
            {
                $m_tag_colour = null;
                foreach ($m_tags as $m_tag)
                {
                    $m_new_tag_colour = $this->get_tag_colour($m_tag_colour);
                    $m_tag_colour = $m_new_tag_colour;
                    $m_tag_name = $m_tag['tag'];
                    $m_tag_id = $m_tag['tagID'];
                    $this->c_html_page_content .= <<< TAG
                    <a href="javascript:submitNavBarForm(document.getElementById('navbarform'),'display_project_tags',$m_tag_id);"><span class="label label-$m_tag_colour">$m_tag_name</span></a>
TAG;
                }
            }
            $this->c_html_page_content .= <<< PROJECT
        </p>
      </div>
PROJECT;
        if ($m_arr_project_details['project_download2_text'] == '')
        {
            $this->c_html_page_content .= <<< PROJECT
      <div class="well">
        <h4>Download<br>
        <small><a href=$m_download1_source $m_download1>$m_download1_text</a></small>
		</h4>
      </div> 
PROJECT;
        }
        $this->c_html_page_content .= <<< PROJECT
    </div>
    <div class="col-sm-6 col-sm-pull-6">
		<h3>$m_please_note</h3>
		$m_description
	</div>
  </div>
PROJECT;
        if ($m_arr_project_details['project_download2_text'] != '')
        {
            $this->c_html_page_content .= <<< PROJECT
  <div class="row">
  <div class="col-sm-6 col-sm-push-6">
      <div class="well">
        <h4>Download<br>
        <small><a href=$m_download1_source $m_download1>$m_download1_text</a></small>
		</h4>
      </div>
	</div>
	<div class="col-sm-6 col-sm-pull-6">
      <div class="well">
        <h4>Download<br>
        <small><a href=$m_download2_source $m_download2>$m_download2_text</a></small>
		</h4>
      </div>  
	  </div>
PROJECT;
        }
        
        $this->c_html_page_content .= <<< PROJECT
  </div>
</div>
</div>

PROJECT;

    }

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    private function get_tag_colour($a_tag_colour)
    {
        switch ($a_tag_colour)
            {
                case 'default':
                    return 'primary';
                  break;
                case 'primary':
                    return 'success';
                  break;
                case 'success':
                    return 'info';
                  break;
                case 'info':
                    return 'warning';
                  break;
                case 'warning':
                    return 'danger';
                  break;
                case 'danger':
                    return 'default';
                  break;  
                default:
                    return 'default';
                  break;
            }        
    }
}
?>
