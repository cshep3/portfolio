<?php
/**
 * class.PortfolioDisplayProjectDetailsModel.php
 *
 * Portfolio website - Chris Shepherd
 *
 * @author Chris Shepherd - chris_shepherd2@hotmail.com
 *
 * @package portfolio
 */

class PortfolioDisplayProjectDetailsModel extends PortfolioModelAbstract
{
    private $c_arr_project_details;
    private $c_sanitised_project_index;

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    public function __construct()
    {
        parent::__construct();
        $this->c_arr_project_details = array();
    }

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    public function get_project_details()
    {
        return $this->c_arr_project_details;
    }

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    public function set_project_index($p_arr_sanitised_input)
    {
        $m_sanitised_project_index = '';
        if (isset($p_arr_sanitised_input['index']))
        {
            $m_sanitised_project_index = $p_arr_sanitised_input['index'];
        }
        $this->c_sanitised_project_index = $m_sanitised_project_index;
    }

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    public function do_retrieve_project_details()
    {
        $m_sanitised_project_index = $this->c_sanitised_project_index;
        $m_sql_query_string = PortfolioSqlQuery::query_get_project_details();
        $m_arr_sql_query_parameters = array(':projectID' => $m_sanitised_project_index);
        $m_query_result = $this->c_obj_database_handle->safe_query($m_sql_query_string, $m_arr_sql_query_parameters);

        $m_project_count = $this->c_obj_database_handle->count_rows();

//        var_dump($this->c_obj_database_handle->get_connection_messages());

        if ($m_project_count == 0)
        {
            $m_sanitised_project_index = false;
        }
        else
        {
            $m_project_details = $this->c_obj_database_handle->safe_fetch_array();
            $this->c_arr_project_details['sanitised-project-index'] = $m_sanitised_project_index;
            $this->c_arr_project_details['project-details'] = $m_project_details;
        }
    }

    public function do_retrieve_project_tags()
    {
        $m_sanitised_project_index = $this->c_sanitised_project_index;
        $m_sql_query_string = PortfolioSqlQuery::query_get_project_tags();
        $m_arr_sql_query_parameters = array(':projectID' => $m_sanitised_project_index);
        
        $m_query_result = $this->c_obj_database_handle->safe_query($m_sql_query_string, $m_arr_sql_query_parameters);

        $m_tag_count = $this->c_obj_database_handle->count_rows();

        if ($m_tag_count == 0)
        {
            $this->c_arr_project_details['project-tags'] = null;
        }
        else
        {
            $this->c_arr_project_details['project-tags'] = $this->c_obj_database_handle->safe_fetch_all_results();
        }
    }
}
?>
