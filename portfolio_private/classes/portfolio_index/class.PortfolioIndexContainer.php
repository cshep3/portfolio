<?php
/**
 * class.PortfolioIndexContainer.php
 *
 * Portfolio website - Chris Shepherd
 *
 * @author Chris Shepherd - chris_shepherd2@hotmail.com
 *
 * @package portfolio
 */

  class PortfolioIndexContainer extends PortfolioContainerAbstract
  {
// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    public function __construct($p_feature_name)
    {
      $m_directory_path = realpath(dirname(__FILE__)) . DIRSEP;
      parent::__construct($p_feature_name, $m_directory_path);
    }

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    public function make_portfolio_index_controller()
    {
        $m_obj_self = $this;
        $m_obj_portfolio_index_controller = new PortfolioIndexController($m_obj_self);
        $m_obj_portfolio_index_controller->do_create_html_output();
        $m_html_output = $m_obj_portfolio_index_controller->get_html_output();
        return $m_html_output;
    }

    public function make_portfolio_index_view()
    {
        $m_obj_portfolio_index_view = new PortfolioIndexView();
        $m_obj_portfolio_index_view->do_create_form();
        $m_html_output = $m_obj_portfolio_index_view->get_html_output();
        return $m_html_output;
    }
  }
?>
