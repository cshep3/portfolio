<?php
  /**
   * class.PortfolioIndexView.php
   *
   * Portfolio website - Chris Shepherd
   *
   * @author Chris Shepherd - chris_shepherd2@hotmail.com
   *
   * @package portfolio
   */

  class PortfolioIndexView extends PortfolioWebPageTemplateView
  {
// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    public function __construct(){}

// // ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    public function do_create_form()
    {
      $this->set_page_title();
      $this->create_page_body();
      $this->create_web_page();
    }

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    protected function set_page_title()
    {
      $this->c_page_title = 'Chris Shepherd Portfolio';
    }

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    private function create_page_body()
    {
      $m_address = APP_ROOT_PATH;

      $m_page_heading = APP_NAME;
      $m_screenshot_source = MEDIA_PATH . "profile.png";

        $this->c_html_page_content = <<< HTML
<div class="container text-center">
<div class="well">
  <div class="row">
	<h1>About Me</h1>
    <div class="col-sm-6 col-sm-push-6">
      <div class="well">
        <img src=$m_screenshot_source class="img-responsive" alt="Profile" style="max-height:300px">
      </div>
      <div class="well">
        <h4>Key Skills</h4>
        <p>
          <span class="label label-default">C++</span>
          <span class="label label-primary">Java</span>
          <span class="label label-success">Object Pascal</span>
          <span class="label label-info">PHP</span>
          <span class="label label-warning">SQL</span>
          <span class="label label-danger">JavaScript</span>
		  <span class="label label-default">HTML</span>
          <span class="label label-primary">CSS</span>
          <span class="label label-success">C#</span>
          <span class="label label-info">Android Development</span>
          <span class="label label-warning">OO Programming</span>
          <span class="label label-danger">Visual Basic</span>
		  <span class="label label-default">OpenGL</span>
        </p>
      </div>
	  <div class="well">
        <a href="https://uk.linkedin.com/pub/chris-shepherd/63/346/847">
        <img src="https://static.licdn.com/scds/common/u/img/webpromo/btn_myprofile_160x33.png" width="160" height="33" border="0" alt="View Chris Shepherd's profile on LinkedIn">        
		</a>
      </div>
    </div>
	
	<div class="col-sm-6 col-sm-pull-6">
		<ul class="nav nav-tabs">
		  <li class="active"><a data-toggle="tab" href="#project">Profile</a></li>
		  <li><a data-toggle="tab" href="#qualifications">Qualifications</a></li>
		  <li><a data-toggle="tab" href="#key-skills">Training</a></li>
		</ul>

		<div class="tab-content">
		  <div id="project" class="tab-pane fade in active">
			<h3>Profile</h3>
			<p>
A highly motivated individual committed to developing my business and technical
skills, with 2 years experience as a software developer.
I am proficient in a range of programming languages such as Java, C++, PHP,
Object Pascal, C#, JavaScript, HTML, CSS and SQL with knowledge of various system
development methodologies.
			</p>
			<p>
I have previously worked on both web and windows applications whilst at university and
professionally at Motor Design Ltd, The Access Group and IBM. Android development
is another area in which I have a lot of knowledge from studying and developing
applications during my university course. I also received extensive training in HTML,
CSS, JavaScript, .NET and SQL Server from my time at The Access Group. As well as extensive
training in Java at IBM.
			</p>
			<p>			
Furthermore I have completed an industrial placement year at Motor Design Ltd. where I
worked as a Software Developer. This involved creating and developing functionality for
their main software package, Motor-CAD, which is written in Object Pascal using Delphi.
My strong work ethic enables me to quickly pick up new skills and apply it to my work.
			</p>
			<p>			
Since October I have worked for the worlds largest technology company, IBM. Since then I have
received world class training, whilst also working on major projects for large public sector
companies.
			</p>			
		  </div>
		  <div id="qualifications" class="tab-pane fade">
			<h3>Qualifications</h3>
			<ul class="text-left">	
			  <li>Computer Games Programming BSc - <span class="text-primary">First Class Honours</span></li>		
				<ul>
					<li>Final Year</li>
					<ul>
					  <li>Mobile Games Development (Android) - <span class="text-primary">83%</span></li>
					  <li>Fuzzy Logic and Knowledge Based Systems (AI) – <span class="text-primary">73%</span></li>
					  <li>Advanced Games Programming (C++) - <span class="text-primary">67%</span></li>					  
					  <li>Final Year Project (PHP) - <span class="text-primary">67%</span></li>					  
					  <li>Secure Web Application Development (PHP)- <span class="text-primary">61%</span></li>
					</ul>	
					<li>Second Year</li>
					<ul>
					  <li>Introduction to Graphics and Interactive 3D Modelling (C++) - <span class="text-primary">69%</span></li>
					  <li>Artificial Intelligence and Modelling for Games (C++) - <span class="text-primary">68%</span></li>
					  <li>C++ for Games Programmers (C++) - <span class="text-primary">60%</span></li>					  
					  <li>Database Design and Implementation (MySQL) - <span class="text-primary">58%</span></li>					 
					</ul>
					<li>First Year</li>
					<ul>
					  <li>C++ Programming (C++) - <span class="text-primary">76%</span></li>
					  <li>Games Architecture, Design and Development (C++) - <span class="text-primary">56%</span></li>
					  <li>Creative Client Computing (UML) - <span class="text-primary">49%</span></li>
					  <li>Computer Systems (C) - <span class="text-primary">40%</span></li>
					</ul>
				</ul>
				<li>A-Levels</li>
					<ul>
					  <li>Computing - <span class="text-primary">B</span></li>
					  <li>Mathematics - <span class="text-primary">B</span></li>
					  <li>Physics - <span class="text-primary">E</span></li>
					</ul>
				<li>10 GCSEs <span class="text-primary">A-C</span>, including Mathematics, English and Science.</li>	
			</ul>
		  </div>
		  <div id="key-skills" class="tab-pane fade">
			<h3>Professional Training</h3>
			<ul class="text-left">
				<li>The Access Group</li>
					<ul>
					  <li>SQL Server</li>
					  <li>HTML/CSS</li>
					  <li>JavaScript</li>
					  <li>C# and .NET</li>
					  <li>Scrum and Kanban</li>
					</ul>
				<li>IBM</li>
					<ul>
					  <li>Core Java</li>					  
					  <li>Agile and Scrum</li>					  
					  <li>Understanding the Spring Framework - Java</li>
					  <li>Brighter Blue: Core skills for success</li>
					  <li>TRIRIGA Application Development Bootcamp</li>
					</ul>	
		  </div>
		</div>	
	</div>
  </div>
</div>
</div>
HTML;

    }
  }
?>
