<?php
  /**
   * class.PortfolioIndexController.php
   *
   * Portfolio website - Chris Shepherd
   *
   * @author Chris Shepherd - chris_shepherd2@hotmail.com
   *
   * @package portfolio
   */

  class PortfolioIndexController extends PortfolioControllerAbstract
  {
// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    public function do_create_html_output()
    {
      $this->c_html_output = $this->c_obj_container->make_portfolio_index_view();
    }
  }
?>
