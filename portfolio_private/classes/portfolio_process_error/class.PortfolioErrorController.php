<?php
/**
 * class.PortfolioErrorController.php
 *
 * Portfolio website - Chris Shepherd
 *
 * @author Chris Shepherd - chris_shepherd2@hotmail.com
 *
 * @package portfolio
 */

 class PortfolioErrorController extends PortfolioControllerAbstract
 {
  private $c_error_type;
  private $c_output_error_message;

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
		public function get_html_output()
		{
			return $this->c_html_output;
		}

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
		public function set_error_type($p_error_type)
  {
   $this->c_error_type = $p_error_type;
  }

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
  public function do_process_error()
  {
   $this->c_output_error_message = $this->c_obj_container->make_portfolio_error_model($this->c_error_type);
  }

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
  public function do_create_output()
  {
   $this->c_html_output = $this->c_obj_container->make_portfolio_error_view($this->c_output_error_message);
  }
  
  public function do_create_html_output() {}

	}
?>
