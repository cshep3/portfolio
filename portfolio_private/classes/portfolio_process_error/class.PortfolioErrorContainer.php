<?php
/**
 * class.PortfolioErrorContainer.php
 *
 * Portfolio website - Chris Shepherd
 *
 * @author Chris Shepherd - chris_shepherd2@hotmail.com
 *
 * @package portfolio
 */

  class PortfolioErrorContainer extends PortfolioContainerAbstract
  {
// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    public function __construct($p_feature_name)
    {
      $m_directory_path = realpath(dirname(__FILE__)) . DIRSEP;
      parent::__construct($p_feature_name, $m_directory_path);
    }

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    public function make_portfolio_error_model($p_error_type)
    {
      $m_obj_error = new PortfolioErrorModel();
      $m_obj_error->set_error_type($p_error_type);
      $m_obj_error->do_select_error_message();
      // $m_obj_error->do_log_error_message();

      $m_error_message = $m_obj_error->get_error_message();
      return $m_error_message;
    }

    public function make_portfolio_error_view($p_output_error_message)
    {
      $m_obj_error = new PortfolioErrorView();
      $m_obj_error->set_error_message($p_output_error_message);
      $m_obj_error->create_error_message();
      $m_error_message = $m_obj_error->get_html_output();
      return $m_error_message;
    }
  }
?>
