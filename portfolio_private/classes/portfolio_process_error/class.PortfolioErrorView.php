<?php
  /**
   * class.PortfolioErrorView.php
   *
   * Portfolio website - Chris Shepherd
   *
   * @author Chris Shepherd - chris_shepherd2@hotmail.com
   *
   * @package portfolio
   */

  class PortfolioErrorView extends PortfolioWebPageTemplateView
  {
    private $c_error_message;
    private $c_obj_db_handle;

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    public function __construct()
    {
      parent::__construct();
      $this->c_error_message = '';
      $this->c_error_message = '';
      $this->c_obj_db_handle = null;
    }

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    public function set_error_message($p_error_message)
    {
      $this->c_error_message = $p_error_message;
    }

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    public function create_error_message()
    {
      $this->set_page_title();
      $this->create_page_body();
      $this->create_web_page();
    }

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    private function set_page_title()
    {
      $m_app_name = APP_NAME;
      $this->c_page_title = $m_app_name . ': processing error...';
    }

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    private function create_page_body()
    {
      $m_address = APP_ROOT_PATH;
      $m_page_heading = 'Portfolio System Error';
      $m_system_message = 'The System Administrator has been notified.';

      $m_html_output = <<< HTML
<div class="container text-center">
<div class="well">
<div class="row">
<h1>$m_page_heading</h1>
<div class="col-sm-12">
<div class="well">
<h3 class="error">$this->error_message</h3>
<p>$m_system_message</p>
<h4><a href=$m_address>Return to Home Page</a></h4>
</div>
</div>
</div>
</div>
</div>
HTML;
      $this->c_html_page_content = $m_html_output;
    }
  }
?>
