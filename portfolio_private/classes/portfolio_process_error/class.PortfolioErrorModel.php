<?php
  /**
   * class.PortfolioErrorModel.php
   *
   * Portfolio website - Chris Shepherd
   *
   * @author Chris Shepherd - chris_shepherd2@hotmail.com
   *
   * @package portfolio
   */

  class PortfolioErrorModel extends PortfolioModelAbstract
  {
    private $c_error_type;
    private $c_output_error_message;

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    public function __construct()
    {
      parent::__construct();
      $this->c_error_type = '';
      $this->c_output_error_message = '';
    }

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    public function get_error_message()
    {
      return $this->c_output_error_message;
    }

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    public function set_error_type($p_error_type)
    {
      $this->c_error_type = $p_error_type;
    }

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    public function do_select_error_message()
    {
      switch ($this->c_error_type)
      {
        case 'class-not-found-error':
        case 'file-not-found-error':
        default:
          $m_error_message = 'Ooops - there was an internal error - please try again later';
          break;
      }
      $this->c_output_error_message = $m_error_message;
    }

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    // public function do_log_error_message()
    // {
    //   $m_user_id = '';

    //   $m_number_of_inserted_messages = 0;

    //   $m_sql_query_string = PortfolioSqlQuery::query_get_error_logging_query_string();
    //   $m_arr_sql_parameters = array(':logmessage' => $this->c_error_type);

    //   $this->c_obj_database_handle->safe_query($m_sql_query_string, $m_arr_sql_parameters);
    //   $m_number_of_inserted_messages = $this->c_obj_database_handle->count_rows();
    // }
  }
?>
