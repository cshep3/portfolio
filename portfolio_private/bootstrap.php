<?php
  /**
   * bootstrap.php
   *
   * Portfolio website - Chris Shepherd
   *
   * @author Chris Shepherd - chris_shepherd2@hotmail.com
   * 
   * Events:
   * 
   * index.php     -> calls Bootstrap
   * bootstrap.php -> starts session
   *               -> autoloads framework files
   *               -> calls PortfolioConfig -> defines constant variables
   *               -> calls PortfolioRouter -> gets feature from HTTP request
   *                                        -> creates relevent Container -> includes MVC files
   *                                                                      -> creates Controller -> calls Validate  -> validates input, passes back to Controller
   *                                                                                            -> passes to Model -> does Processing, returns result to Controller
   *                                                                                            -> passes to View  -> create relevent output page, returns to Controller
   *                                                                                            -> returns result to Router
   *                                        -> creates ProcessOutput      -> echo HTML result
   * 
   *
   * @package portfolio
   */

  session_start();
  include_once 'classes/portfolio_framework/class.PortfolioConfig.php';
  PortfolioConfig::do_definitions();
  $html_result = PortfolioContainer::make_portfolio_router();

// end of application code

// ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
  function __autoload($p_class_name)
  {
    $m_error_count = 0;
    $m_file_exists = false;
    $m_class_exists = true;
    $m_file_name = CLASS_FILE . $p_class_name . '.php';
    $m_arr_directories = array_diff(scandir(CLASS_PATH), array('..', '.'));

    foreach ($m_arr_directories as $m_directory)
    {
      $m_file_path_and_name = CLASS_PATH . $m_directory . DIRSEP . $m_file_name;
      if (file_exists($m_file_path_and_name))
      {
        $m_file_exists = true;
        break;
      }
    }

    if ($m_file_exists)
    {
      require_once $m_file_path_and_name;
      if (!class_exists($p_class_name))
      {
        $m_class_exists = false;
        $m_error_count++;
      }
    }
    else
    {
      $m_error_count++;
    }

    if ($m_error_count > 0)
    {
      if (!$m_file_exists)
      {
        $error_message = PortfolioContainer::make_portfolio_error('file-not-found-error');
      }
      if (!$m_class_exists)
      {
        $error_message = PortfolioContainer::make_portfolio_error('class-not-found-error');
      }

      PortfolioContainer::make_portfolio_process_output($error_message);
    }
  }
?>
