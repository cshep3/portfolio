#DROP DATABASE `portfolio_db`;
#CREATE DATABASE `portfolio_db`;

DROP USER 'portfoliouser'@'localhost';
FLUSH PRIVILEGES;
CREATE USER 'portfoliouser'@'localhost' IDENTIFIED BY 'portfolio_pass123';
GRANT SELECT, INSERT, UPDATE ON `portfolio_db`.* TO 'portfoliouser'@'localhost';
GRANT DELETE ON `portfolio_db`.* TO 'portfoliouser'@'localhost';

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `menu_section`
-- ----------------------------
DROP TABLE IF EXISTS `menu_section`;
CREATE TABLE `menu_section` (
  `menu_sectionID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menu_section_title` varchar(20) NOT NULL,
  PRIMARY KEY (`menu_sectionID`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of menu_section
-- ----------------------------
INSERT INTO `menu_section` VALUES ('1', 'C++');
INSERT INTO `menu_section` VALUES ('2', 'Java');
INSERT INTO `menu_section` VALUES ('3', 'PHP');
INSERT INTO `menu_section` VALUES ('4', 'Matlab');
INSERT INTO `menu_section` VALUES ('5', 'Visual Basic');
INSERT INTO `menu_section` VALUES ('6', 'Placement Year');
INSERT INTO `menu_section` VALUES ('7', 'Employment History');

-- ----------------------------
-- Table structure for `tag`
-- ----------------------------
DROP TABLE IF EXISTS `tag`;
CREATE TABLE `tag` (
  `tagID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tag` varchar(20) NOT NULL,
  PRIMARY KEY (`tagID`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tag
-- ----------------------------
INSERT INTO `tag` VALUES ('1', 'C++');
INSERT INTO `tag` VALUES ('2', 'Java');
INSERT INTO `tag` VALUES ('3', 'PHP');
INSERT INTO `tag` VALUES ('4', 'Matlab');
INSERT INTO `tag` VALUES ('5', 'Visual Basic');
INSERT INTO `tag` VALUES ('6', 'JavaScript');
INSERT INTO `tag` VALUES ('7', 'JQuery');
INSERT INTO `tag` VALUES ('8', 'HTML');
INSERT INTO `tag` VALUES ('9', 'CSS');
INSERT INTO `tag` VALUES ('10', 'AJAX');
INSERT INTO `tag` VALUES ('11', 'SFML');
INSERT INTO `tag` VALUES ('12', 'OpenGL');
INSERT INTO `tag` VALUES ('13', 'Android');
INSERT INTO `tag` VALUES ('14', 'AI');
INSERT INTO `tag` VALUES ('15', 'Web Services');
INSERT INTO `tag` VALUES ('16', 'OO');
INSERT INTO `tag` VALUES ('17', 'MVC Architecture');
INSERT INTO `tag` VALUES ('18', 'Object Pascal');
INSERT INTO `tag` VALUES ('19', 'Quicksort');
INSERT INTO `tag` VALUES ('20', 'Fuzzy Logic');
INSERT INTO `tag` VALUES ('21', 'Games');
INSERT INTO `tag` VALUES ('22', '2D');
INSERT INTO `tag` VALUES ('23', '3D');
INSERT INTO `tag` VALUES ('24', 'A* Algorithm');
INSERT INTO `tag` VALUES ('25', 'BDD');
INSERT INTO `tag` VALUES ('26', 'TDD');
INSERT INTO `tag` VALUES ('27', 'Encryption');
INSERT INTO `tag` VALUES ('28', 'Hashing');
INSERT INTO `tag` VALUES ('29', 'Captcha');

-- ----------------------------
-- Table structure for `project`
-- ----------------------------
DROP TABLE IF EXISTS `project`;
CREATE TABLE `project` (
  `projectID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `project_title` varchar(20) NOT NULL,
  `screenshot_source` varchar(255) NOT NULL,
  `menu_section` int(10) unsigned NOT NULL,
  `menu_title` varchar(20) NOT NULL,
  `menu_thumbnail_source` varchar(255) NOT NULL,
  `project_description` text NOT NULL,
  `project_please_note` text NOT NULL,
  `project_download1_text` varchar(20) NOT NULL,
  `project_download1_source` varchar(255) NOT NULL,
  `project_download2_text` varchar(20) NOT NULL,
  `project_download2_source` varchar(255) NOT NULL,
  PRIMARY KEY (`projectID`),
  FOREIGN KEY (`menu_section`) REFERENCES `menu_section`(`menu_sectionID`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for `project_tag`
-- ----------------------------
DROP TABLE IF EXISTS `project_tag`;
CREATE TABLE `project_tag` (
  `tagID` int(10) unsigned NOT NULL,
  `projectID` int(10) unsigned NOT NULL,
  PRIMARY KEY (`tagID`,`projectID`),
  FOREIGN KEY (`tagID`) REFERENCES `tag`(`tagID`),
  FOREIGN KEY (`projectID`) REFERENCES `project`(`projectID`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for `job`
-- ----------------------------
DROP TABLE IF EXISTS `job`;
CREATE TABLE `job` (
  `jobID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `company_name` varchar(20) NOT NULL,
  `menu_section` int(10) unsigned NOT NULL,
  `menu_title` varchar(20) NOT NULL,
  `job_title` varchar(20) NOT NULL,
  `dates` varchar(30) NOT NULL,
  `job_description` text NOT NULL,
  `company_website` varchar(30),
  PRIMARY KEY (`jobID`)
#  FOREIGN KEY (`menu_section`) REFERENCES `menu_section`(`menu_sectionID`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for `job_tag`
-- ----------------------------
DROP TABLE IF EXISTS `job_tag`;
CREATE TABLE `job_tag` (
  `tagID` int(10) unsigned NOT NULL,
  `jobID` int(10) unsigned NOT NULL,
  PRIMARY KEY (`tagID`,`jobID`),
  FOREIGN KEY (`tagID`) REFERENCES `tag`(`tagID`),
  FOREIGN KEY (`jobID`) REFERENCES `job`(`jobID`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for `placement`
-- ----------------------------
DROP TABLE IF EXISTS `placement`;
CREATE TABLE `placement` (
  `placementID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `company_name` varchar(20) NOT NULL,
  `job_title` varchar(20) NOT NULL,
  `dates` varchar(30) NOT NULL,
  `job_description` text NOT NULL,
  `company_website` varchar(30),
  `download_source` varchar(255) NOT NULL,
  PRIMARY KEY (`placementID`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for `placement_tag`
-- ----------------------------
DROP TABLE IF EXISTS `placement_tag`;
CREATE TABLE `placement_tag` (
  `tagID` int(10) unsigned NOT NULL,
  `placementID` int(10) unsigned NOT NULL,
  PRIMARY KEY (`tagID`,`placementID`),
  FOREIGN KEY (`tagID`) REFERENCES `tag`(`tagID`),
  FOREIGN KEY (`placementID`) REFERENCES `placement`(`placementID`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for `portfolio_error_log`
-- ----------------------------
DROP TABLE IF EXISTS `portfolio_error_log`;
CREATE TABLE `portfolio_error_log` (
  `logID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `log_message` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `log_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`logID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;