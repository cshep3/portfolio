/**
 * portfolio_functions.js
 *
 * Portfolio website - Chris Shepherd
 *
 * @author Chris Shepherd - chris_shepherd2@hotmail.com
 *
 * @package portfolio
 */

function submitNavBarForm(form, fVal, iVal) {

    form.elements["feature"].value = fVal;
    form.elements["index"].value = iVal;

    // Finally submit the form.
    form.submit();
}