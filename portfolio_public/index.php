<?php
/**
* index.php
*
* Portfolio website - Chris Shepherd
*
* @author Chris Shepherd - chris_shepherd2@hotmail.com
*
* @package portfolio
*/

ini_set('display_errors', 'on');
ini_set('html_errors', 'on');
ini_set('xdebug.trace_output_name', 'portfolio.%t');
xdebug_start_trace();
//Force HTTPS
if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') {
    header('Location: https://' . $_SERVER["SERVER_NAME"] . $_SERVER['REQUEST_URI']);
}
include '../portfolio_private/bootstrap.php';
xdebug_stop_trace();
?>